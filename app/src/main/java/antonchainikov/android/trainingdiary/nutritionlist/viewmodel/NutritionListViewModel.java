package antonchainikov.android.trainingdiary.nutritionlist.viewmodel;


import java.util.Comparator;

import antonchainikov.android.trainingdiary.genericviewmodel.SearchableRecyclerViewModel;
import antonchainikov.android.trainingdiary.diarydata.Food;

public class NutritionListViewModel extends SearchableRecyclerViewModel<Food> {
    @Override
    protected Comparator<Food> getComparator() {
        return Food.getComparator();
    }
}
