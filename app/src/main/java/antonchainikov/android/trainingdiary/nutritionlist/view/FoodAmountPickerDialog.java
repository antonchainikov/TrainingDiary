package antonchainikov.android.trainingdiary.nutritionlist.view;

import android.app.Activity;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import antonchainikov.android.trainingdiary.databinding.NutritionDialogBinding;
import antonchainikov.android.trainingdiary.diarydata.Food;
import antonchainikov.android.trainingdiary.R;
import antonchainikov.android.trainingdiary.nutritionlist.viewmodel.FoodAmountPickerDialogViewModel;
import antonchainikov.android.trainingdiary.utils.ViewUtils;


public class FoodAmountPickerDialog extends DialogFragment {

    private static final String TAG = "FoodAmountPickerDialog";
    private final static String FOOD_ARG = "antonchainikov.android.trainingdiary.nutritionList.view.food_arg";

    private FoodAmountPickerDialogViewModel mViewModel;
    private NutritionDialogBinding mBinding;

    public static FoodAmountPickerDialog getDialog(Food food){
        FoodAmountPickerDialog dialog = new FoodAmountPickerDialog();
        Bundle args = new Bundle(1);
        args.putParcelable(FOOD_ARG, food);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    @NonNull
    @SuppressWarnings("unchecked")
    public Dialog onCreateDialog( Bundle savedInstanceState){
        View v = LayoutInflater.from(getActivity())
                .inflate(R.layout.nutrition_dialog, null);
        mBinding = DataBindingUtil.bind(v);
        Food food = null;
        Bundle args = getArguments();
        if (args != null) {
            food = args.getParcelable(FOOD_ARG);
            if (food == null) {
                food = Food.getStub();
            }
        }
        mBinding.setFood(food);
        mViewModel = ViewModelProviders.of(this).get(FoodAmountPickerDialogViewModel.class);
        mViewModel.init(food);
        mViewModel.getDismissTask().observe(this, getDismissTaskObserver());
        mViewModel.getReturnResultTask().observe(this, getReturnResultTaskObserver());
        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle(mViewModel.getName())
                .setPositiveButton(android.R.string.ok, new PositiveDialogOnClickListener())
                .setNegativeButton(android.R.string.cancel, new NegativeDialogOnClickListener())
                .create();
    }



    private class PositiveDialogOnClickListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            String errMsg = getContext().getString(R.string.empty_textfield_error_msg);
            EditText amountEditText = mBinding.nutritionDialogEditText;
            if (ViewUtils.validateInput(errMsg, amountEditText)) {
                mViewModel.onPositiveButtonClick(ViewUtils.getDoubleFromText(amountEditText));
            }
        }
    }

    private class NegativeDialogOnClickListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            mViewModel.onNegativeButtonClick();
        }
    }

    private Observer<Food> getReturnResultTaskObserver() {
        return new Observer<Food>() {
            @Override
            public void onChanged(@Nullable Food food) {
                getActivity().setResult(Activity.RESULT_OK,
                        new Intent().putExtra(
                                Intent.EXTRA_RETURN_RESULT,
                                food
                        )
                );
                getActivity().finish();
            }
        };
    }

    private Observer getDismissTaskObserver() {
        return new Observer() {
            @Override
            public void onChanged(@Nullable Object o) {
                dismiss();
            }
        };
    }

}
