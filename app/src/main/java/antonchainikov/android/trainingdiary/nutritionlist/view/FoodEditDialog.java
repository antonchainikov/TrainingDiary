package antonchainikov.android.trainingdiary.nutritionlist.view;

import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.EditText;

import antonchainikov.android.trainingdiary.R;
import antonchainikov.android.trainingdiary.databinding.NutionEditDialogBinding;
import antonchainikov.android.trainingdiary.diarydata.Food;
import antonchainikov.android.trainingdiary.genericviewmodel.EditDialogViewModel;
import antonchainikov.android.trainingdiary.genericviewmodel.OnItemEditedListener;
import antonchainikov.android.trainingdiary.utils.ViewUtils;

public class FoodEditDialog extends DialogFragment
        implements DialogInterface.OnClickListener {

    private final static String TAG = "FoodEditDialog";
    private final static String LISTENER_ARG = "antonchainikov.android.trainingdiary.nutritionList.view.listener.arg";
    private final static String FOOD_ARG = "antonchainikov.android.trainingdiary.nutritionList.view.food.arg";


    private NutionEditDialogBinding mBinding;
    private EditDialogViewModel<Food> mViewModel;


    public static FoodEditDialog getInstance(OnItemEditedListener<Food> onItemEditedListener) {
        FoodEditDialog dialog = new FoodEditDialog();
        dialog.setArguments(prepareArguments(null, onItemEditedListener));
        return dialog;
    }

    public static FoodEditDialog getInstance(Food food, OnItemEditedListener<Food> listener) {
        FoodEditDialog dialog = new FoodEditDialog();
        dialog.setArguments(prepareArguments(food, listener));
        return dialog;
    }

    private static Bundle prepareArguments(Food food, OnItemEditedListener<Food> listener) {
        Bundle arguments = new Bundle(2);
        if (food != null) {
            arguments.putParcelable(FOOD_ARG, food);
        }
        arguments.putSerializable(LISTENER_ARG, listener);
        return arguments;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        mBinding = DataBindingUtil
                .inflate(inflater, R.layout.nution_edit_dialog, null, false);

        initViewModel();

        return new AlertDialog.Builder(context)
                .setView(mBinding.getRoot())
                .setPositiveButton(android.R.string.ok, this)
                .setNegativeButton(android.R.string.cancel, this)
                .create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE: {
                onPositiveButtonClick();
            }
            case DialogInterface.BUTTON_NEGATIVE: {
                onNegativeButtonClick();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public void initViewModel() {
        Log.e(TAG, "VM is initialising");
        mViewModel = ViewModelProviders.of(this).get(EditDialogViewModel.class);
        setViewModelArgs(getArguments());
        mViewModel.init();
        mViewModel.getDismissTask().observe(this, getOnDismissTaskObserver());
    }

    @SuppressWarnings("unchecked")
    private void setViewModelArgs(Bundle args) {
        if (args != null) {
            Food food = args.getParcelable(FOOD_ARG);
            OnItemEditedListener<Food> listener = (OnItemEditedListener<Food>)args.getSerializable(LISTENER_ARG);
            mViewModel.setItemEditedListener(listener);
            mViewModel.setItem(food);
            mBinding.setFood(food);
        }
    }

    private Observer getOnDismissTaskObserver() {
        return new Observer() {
            @Override
            public void onChanged(@Nullable Object o) {
                dismiss();
            }
        };
    }

    private void onPositiveButtonClick() {
        EditText nameEditText = mBinding.nutritionEditNameEditText;
        EditText caloriesEditText = mBinding.nutritionEditCaloriesEditText;
        EditText proteinEditText = mBinding.nutritionEditProteinEditText;
        EditText fatEditText = mBinding.nutritionEditFatEditText;
        EditText carbsEditText = mBinding.nutritionEditCarbsEditText;

        String errMsg = getContext().getString(R.string.empty_textfield_error_msg);
        boolean correctInput = ViewUtils.validateInput(
                errMsg,
                nameEditText,
                caloriesEditText,
                proteinEditText,
                fatEditText,
                carbsEditText
        );

        if (correctInput) {
            Food food = new Food(
                    nameEditText.getText().toString().trim(),
                    ViewUtils.getDoubleFromText(proteinEditText),
                    ViewUtils.getDoubleFromText(fatEditText),
                    ViewUtils.getDoubleFromText(carbsEditText),
                    ViewUtils.getDoubleFromText(caloriesEditText));
            mViewModel.onPositiveButtonClick(food);
        }
    }

    private void onNegativeButtonClick() {
        mViewModel.onNegativeButtonClick();
    }

}
