package antonchainikov.android.trainingdiary.nutritionlist.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import antonchainikov.android.trainingdiary.SingleFragmentActivity;
import antonchainikov.android.trainingdiary.diarydata.DiaryDatabase;
import antonchainikov.android.trainingdiary.diarydata.Food;
import antonchainikov.android.trainingdiary.nutritionlist.viewmodel.FoodDiffCallback;
import antonchainikov.android.trainingdiary.nutritionlist.viewmodel.NutritionListViewModel;
import antonchainikov.android.trainingdiary.genericviewmodel.SearchCriteria;

public class NutritionListActivity extends SingleFragmentActivity {

    private NutritionListViewModel mViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static Intent makeStartIntent(Context context){
        return new Intent(context, NutritionListActivity.class);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Fragment createFragment() {
        mViewModel = ViewModelProviders.of(this).get(NutritionListViewModel.class);
        mViewModel.setSearchCriteria(new SearchCriteria<Food>() {
            @Override
            public boolean meetsCriteria(Food o1, String query) {
                return o1.getName().toLowerCase().contains(query.toLowerCase());
            }
        });
        mViewModel.setDiffCallback(new FoodDiffCallback());
        mViewModel.init(DiaryDatabase.getInstance(this).getFoodDataset(null));
        return NutritionListFragment.getInstance(mViewModel);
    }
}
