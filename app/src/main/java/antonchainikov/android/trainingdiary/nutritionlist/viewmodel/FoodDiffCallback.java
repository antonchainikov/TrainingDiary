package antonchainikov.android.trainingdiary.nutritionlist.viewmodel;


import antonchainikov.android.trainingdiary.genericviewmodel.CustomDiffCallback;
import antonchainikov.android.trainingdiary.diarydata.Food;

public class FoodDiffCallback extends CustomDiffCallback<Food> {

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        long oldListItemID = mOldList.get(oldItemPosition).getId();
        long newListItemID = mNewList.get(newItemPosition).getId();
        return oldListItemID == newListItemID;
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldList.get(oldItemPosition).equals(mNewList.get(newItemPosition));
    }
}
