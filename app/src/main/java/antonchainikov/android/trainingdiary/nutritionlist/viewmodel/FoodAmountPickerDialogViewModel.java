package antonchainikov.android.trainingdiary.nutritionlist.viewmodel;

import android.arch.lifecycle.ViewModel;

import antonchainikov.android.trainingdiary.LiveTask;
import antonchainikov.android.trainingdiary.diarydata.Food;

public class FoodAmountPickerDialogViewModel extends ViewModel {

    private LiveTask mDismissTask;
    private LiveTask<Food> mReturnResultTask;

    private Food mItem;

    public void init(Food item) {
        mItem = item;
        mDismissTask = new LiveTask();
        mReturnResultTask = new LiveTask<>();
    }

    public String getName() {
        return mItem.getName();
    }

    public void onPositiveButtonClick(Double amount) {
        mItem.setAmount(amount);
        mReturnResultTask.executeTask(mItem);
    }

    public void onNegativeButtonClick() {
        mDismissTask.executeTask();
    }

    public LiveTask getDismissTask() {
        return mDismissTask;
    }

    public LiveTask<Food> getReturnResultTask() {
        return mReturnResultTask;
    }
}
