package antonchainikov.android.trainingdiary.nutritionlist.view;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import antonchainikov.android.trainingdiary.databinding.NutritionListFragmentBinding;
import antonchainikov.android.trainingdiary.databinding.ListItemFoodBinding;
import antonchainikov.android.trainingdiary.diarydata.Food;
import antonchainikov.android.trainingdiary.R;
import antonchainikov.android.trainingdiary.genericviewlayer.DiaryAdapter;
import antonchainikov.android.trainingdiary.genericviewmodel.RecyclerViewModel;
import antonchainikov.android.trainingdiary.genericviewmodel.SearchableViewModel;

public class NutritionListFragment extends Fragment implements ActionMode.Callback{

    private SearchView mSearchView;
    private ActionMode mActionMode;

    private final static String TAG = "NutritionListFragment";
    private final static int EDIT_ITEM_REQUEST = -1;

    private NutritionListFragmentBinding mBinding;
    private SearchableViewModel<Food> mViewModel;

    public static NutritionListFragment getInstance(SearchableViewModel<Food> viewModel) {
        NutritionListFragment fragment = new NutritionListFragment();
        fragment.setViewModel(viewModel);
        return fragment;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mViewModel.getOpenEditDialogTask().observe(this, getOpenEditDialogTaskObserver());
        mViewModel.getOpenAddDialogTask().observe(this, getOpenAddDialogTaskObserver());
        mViewModel.getChangeEditButtonVisibilityTask().observe(this, getEditButtonVisibilityChangeObserver());
        mViewModel.getChangeDeleteButtonVisibilityTask().observe(this, getDeleteButtonVisibilityChangeObserver());
        mViewModel.getRedrawItemTask().observe(this, getRedrawItemTaskObserver());
        mViewModel.getOpenAmountPickerDialogTask().observe(this, getOpenAmountPickerDialogTask());
    }

    public void setViewModel(SearchableViewModel<Food> viewModel) {
        mViewModel = viewModel;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle onSavedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.nutrition_list_fragment,
                container,
                false
        );
        mBinding.setViewModel(mViewModel);
        setupRecyclerView();
        return mBinding.getRoot();
    }

    private Observer<Food> getOpenAmountPickerDialogTask() {
        return new Observer<Food>() {
            @Override
            public void onChanged(@Nullable Food food) {
                DialogFragment dialog = FoodAmountPickerDialog.getDialog(food);
                dialog.show(getFragmentManager(), "");
            }
        };
    }

    private Observer<Food> getOpenEditDialogTaskObserver() {
        return new Observer<Food>() {
            @Override
            public void onChanged(@Nullable Food food) {
                DialogFragment dialog = FoodEditDialog.getInstance(food, mViewModel);
                dialog.show(getFragmentManager(), "");
            }
        };
    }

    private Observer getOpenAddDialogTaskObserver() {
        return new Observer() {
            @Override
            public void onChanged(Object o) {
                DialogFragment dialog = FoodEditDialog.getInstance(mViewModel);
                dialog.show(getFragmentManager(), "");
            }
        };
    }

    private Observer<Boolean> getEditButtonVisibilityChangeObserver() {
        return new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean visible) {
                MenuItem item = mActionMode.getMenu().findItem(R.id.edit_button_item);
                if (item != null) {
                    item.setEnabled(visible);
                }
            }
        };
    }

    private Observer<Boolean> getDeleteButtonVisibilityChangeObserver() {
        return new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean visible) {
                Log.d(TAG, "Changing visibility to " + visible);
                if (visible) {
                    mActionMode = getActivity().startActionMode(NutritionListFragment.this);
                } else {
                    Log.d(TAG, "Closing action mode");
                    mActionMode.finish();
                }
            }
        };
    }

    private Observer<Integer> getRedrawItemTaskObserver() {
        return new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer position) {
                RecyclerView.Adapter adapter = mBinding.nutritionListRecyclerView.getAdapter();
                if (position != null && position >= 0 && position < adapter.getItemCount()) {
                    adapter.notifyItemChanged(position);
                }
            }
        };
    }

    private void setupRecyclerView() {
        RecyclerView recyclerView = mBinding.nutritionListRecyclerView;
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        NutritionListAdapter adapter = new NutritionListAdapter(mViewModel, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.menu_layout, menu);
        MenuItem searchItem = menu.findItem(R.id.menu_item_app_bar_search);
        mSearchView = (SearchView) searchItem.getActionView();
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return mViewModel.onSearchQueryChanged(query);
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return mViewModel.onSearchQueryChanged(newText);
            }
        });

    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.action_mode, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.action_mode, menu);
        return true;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch(item.getItemId()) {
            case R.id.delete_button_item:
            {
                mViewModel.onDeleteItemsClick();
                return true;
            }
            case R.id.edit_button_item:
            {
                mViewModel.onEditItemClick();
                return true;
            }
            default:
            {
                return false;
            }
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        Log.d(TAG, "onDestroyActionMode called");
        mViewModel.onActionModeCanceled();
    }

    private static class NutritionListAdapter extends DiaryAdapter<Food, ListItemFoodBinding> {
        NutritionListAdapter(RecyclerViewModel<Food> viewModel,
                             LifecycleOwner lifecycleOwner ) {
            super(viewModel, lifecycleOwner);
        }


        @Override
        protected int getLayoutToInflate(int itemViewType) {
            return R.layout.list_item_food;
        }
    }

}
