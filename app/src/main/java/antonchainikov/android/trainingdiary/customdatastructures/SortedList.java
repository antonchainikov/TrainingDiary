package antonchainikov.android.trainingdiary.customdatastructures;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class SortedList<E> extends ArrayList<E> {

    private Comparator<E> mComparator;

    public SortedList(Comparator<E> comparator) {
        mComparator = comparator;
    }

    public SortedList(int size, Comparator<E> comparator) {
        super(size);
        mComparator = comparator;
    }

    public SortedList(Collection<? extends E> collection, Comparator<E> comparator) {
        super(collection);
        mComparator = comparator;
    }

    @Override
    public void add(int index, E element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean add(E element) {
        int pos;
        if (mComparator != null) {
            pos = Collections.binarySearch(this, element, mComparator);
        } else {
            return false;
        }
        if (pos < 0) {
            pos = -pos - 1;
        }
        int size = size();
        super.add(pos, element);
        if (size() > size) {
            return true;
        }
        return false;
    }
}
