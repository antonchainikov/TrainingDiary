package antonchainikov.android.trainingdiary;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import antonchainikov.android.trainingdiary.diarydata.Exercise;


public class TrainingPlanCreationDialog extends DialogFragment
        implements DialogInterface.OnClickListener{

    private final static String TAG = "PlanCreationDialog";

    private String mExerciseName;
    private String mExerciseDescr;
    private long mExerciseId;
    private EditText mWeightEditText;
    private EditText mRepsEditText;
    private EditText mSetsEditText;
    private EditText mTimeoutEditText;

    public static TrainingPlanCreationDialog getDialog(
            String name,
            String description,
            long id) {

        TrainingPlanCreationDialog dialog = new TrainingPlanCreationDialog();
        dialog.setExerciseData(name, description, id);
        return dialog;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState){
        View v = LayoutInflater.from(getContext())
                .inflate(R.layout.create_training_plan_dialog, null);
        mWeightEditText = v.findViewById(R.id.create_training_plan_dialog_weight_EditText);
        mRepsEditText = v.findViewById(R.id.create_training_plan_dialog_reps_EditText);
        mSetsEditText = v.findViewById(R.id.create_training_plan_dialog_sets_EditText);
        mTimeoutEditText = v.findViewById(R.id.create_training_plan_dialog_timeout_EditText);
        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle(mExerciseName)
                .setPositiveButton(android.R.string.ok, this)
                .setNegativeButton(android.R.string.cancel, null)
                .create();
    }

    private void setExerciseData(String name, String description, long id) {
        mExerciseName = name;
        mExerciseDescr = description;
        mExerciseId = id;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            boolean filledFields =
                            mWeightEditText.getText().length() > 0 &&
                            mRepsEditText.getText().length() > 0 &&
                            mSetsEditText.getText().length() > 0 &&
                            mTimeoutEditText.getText().length() > 0;
            if (filledFields) {
                Bundle argumentsBundle = Exercise.createArgumentsBundle(
                        mExerciseName,
                        mExerciseDescr,
                        mExerciseId,
                        Double.parseDouble(mWeightEditText.getText().toString()),
                        Integer.parseInt(mRepsEditText.getText().toString()),
                        Integer.parseInt(mSetsEditText.getText().toString()),
                        Double.parseDouble(mTimeoutEditText.getText().toString())
                );
                Intent trainingPlanData = new Intent();
                trainingPlanData.putExtra(Intent.EXTRA_RETURN_RESULT, argumentsBundle);
                getActivity().setResult(Activity.RESULT_OK, trainingPlanData);
                getActivity().finish();
            }
        }
    }


}
