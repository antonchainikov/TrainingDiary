package antonchainikov.android.trainingdiary;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import antonchainikov.android.trainingdiary.menuforday.view.DietActivity;
import antonchainikov.android.trainingdiary.menuforday.view.DietForDayActivity;

public class MainMenuFragment extends Fragment {

    private final static String TAG = "MainMenuFragment";

    private Button mOpenDietButton;
    private Button mOpenTrainingButton;

    @Override
    public View onCreateView(LayoutInflater inflater,
                         ViewGroup container,
                         Bundle onSavedInstanceState){

        View v = inflater.inflate(R.layout.main_menu_fragment, container, false);

        initDietButton(v);
        initTrainingButton(v);

        return v;
    }

    private void initDietButton(View view){
        mOpenDietButton = (Button) view.findViewById(R.id.open_diet_button);
        mOpenDietButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = DietForDayActivity.makeStartIntent(getActivity());
                startActivity(startIntent);
            }
        });
    }

    private void initTrainingButton(View view){
        mOpenTrainingButton = (Button) view.findViewById(R.id.open_training_button);
        mOpenTrainingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = TrainingListActivity.makeStartIntent(getActivity());
                startActivity(startIntent);
            }
        });
    }

}
