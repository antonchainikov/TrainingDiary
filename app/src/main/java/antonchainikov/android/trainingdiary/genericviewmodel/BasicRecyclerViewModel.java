package antonchainikov.android.trainingdiary.genericviewmodel;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.util.Log;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import antonchainikov.android.trainingdiary.customdatastructures.SortedList;
import antonchainikov.android.trainingdiary.LiveTask;
import antonchainikov.android.trainingdiary.diarydata.DataWrapper;

public abstract class BasicRecyclerViewModel<T> extends ViewModel
        implements DataWrapper.OnLoadCompleteListener<T>,
        RecyclerViewModel<T>,
        OnItemEditedListener<T> {

    private final static String TAG = "BasicRecyclerViewModel";

    protected List<T> mItemsList;

    private LiveTask<T> mOpenAddDialogTask;
    private LiveTask<T> mOpenEditDialogTask;
    private LiveTask<T> mOpenAmountPickerDialogTask;
    private LiveTask<UpdatedData<T>> mUpdateAdapterTask;

    private MultiSelect<T> mMultiselect;

    protected DataWrapper<T> mDataset;

    private CustomDiffCallback<T> mDiffCallback;

    public void init(@NonNull DataWrapper<T> dataset) {

        mUpdateAdapterTask = new LiveTask<>();
        mOpenAddDialogTask = new LiveTask<>();
        mOpenEditDialogTask = new LiveTask<>();
        mOpenAmountPickerDialogTask = new LiveTask<>();

        mDataset = dataset;
        mDataset.getAll(this);
        mMultiselect = MultiSelect.getInstance();
    }

    @Override
    public LiveTask<T> getOpenEditDialogTask() {
        return mOpenEditDialogTask;
    }

    @Override
    public LiveTask getOpenAddDialogTask() {
        return mOpenAddDialogTask;
    }

    @Override
    public LiveTask<T> getOpenAmountPickerDialogTask() {
        return mOpenAmountPickerDialogTask;
    }


    @Override
    public LiveTask<UpdatedData<T>> getUpdateAdapterTask() {
        return mUpdateAdapterTask;
    }

    @Override
    public LiveTask<Boolean> getChangeDeleteButtonVisibilityTask() {
        return mMultiselect.getChangeElementsMenuVisibilityTask();
    }

    @Override
    public LiveTask<Boolean> getChangeEditButtonVisibilityTask() {
        return mMultiselect.getChangeEditButtonVisibilityTask();
    }

    @Override
    public LiveTask<Integer> getRedrawItemTask() {
        return mMultiselect.getRedrawItemTask();
    }

    @Override
    public void onLoadComplete(final List<T> data) {
        mItemsList = new SortedList<T>(data, getComparator());
        if (mDiffCallback != null) {
            mDiffCallback.setNewList(mItemsList);
        }
        mUpdateAdapterTask.executeTask(prepareDataToReturn(mItemsList,null));
    }

    protected abstract Comparator<T> getComparator();

    @Override
    public void onRecyclerViewItemClick(int position) {
        if (!mMultiselect.isInSelectionState()) {
            List<T> list = mItemsList;
            if (list != null) {
                T item = list.get(position);
                mOpenAmountPickerDialogTask.executeTask(item);
            }
        } else if (mMultiselect.isItemSelected(position)) {
            mMultiselect.toggleSelection(position);
        }
    }

    @Override
    public boolean onRecyclerViewItemLongClick(int position) {
        mMultiselect.toggleSelection(position);
        return true;
    }

    @Override
    public boolean isItemSelected(int position) {
        return mMultiselect.isItemSelected(position);
    }

    @Override
    public void onDeleteItemsClick() {
        Iterator<T> itemToDelete = mMultiselect.getSelectedItems(mItemsList);
        while (itemToDelete.hasNext()) {
            T item = itemToDelete.next();
            mDataset.delete(item);
            mItemsList.remove(item);
        }
        if (mDiffCallback != null) {
            mDiffCallback.setNewList(mItemsList);
            mUpdateAdapterTask.executeTask(prepareDataToReturn(mItemsList, mDiffCallback));
        } else {
            Log.e(TAG, "DiffCallback is null, unable to dispatch result");
        }
        mMultiselect.clear();
    }

    @Override
    public void onEditItemClick() {
        if (mMultiselect.isItemEditAvailable()) {
            T item = mMultiselect.extractSelectedItem(mItemsList);
            if (item == null) {
                Log.e(TAG, "Selected item appears to be null, skipping edit event");
                return;
            }
            mOpenEditDialogTask.executeTask(item);
        } else {
            Log.e(TAG, "Multiselect is in an inappropriate state, skipping edit event");
        }

    }

    @Override
    public void onAddItemClick() {
        mOpenAddDialogTask.executeTask();
    }


    public void setDiffCallback(CustomDiffCallback<T> diffCallback) {
        mDiffCallback = diffCallback;
    }

    @Override
    public void onActionModeCanceled() {
        mMultiselect.clear();
    }

    @Override
    public void onItemEdited(T oldItem, T newItem) {
        try {
            mDataset.add(newItem);
            mItemsList.remove(oldItem);
            mDiffCallback.setNewList(mItemsList);
            mUpdateAdapterTask.executeTask(prepareDataToReturn(mItemsList, mDiffCallback));
            mItemsList.add(newItem);
            mDiffCallback.setNewList(mItemsList);
            mUpdateAdapterTask.executeTask(prepareDataToReturn(mItemsList, mDiffCallback));
        } catch (IOException e) {
            Log.e(TAG, "Error adding item");
        }
    }

    protected UpdatedData<T> prepareDataToReturn(List<T> list, CustomDiffCallback<T> cb) {
        return prepareDataToReturn(list, cb, false);
    }

    protected UpdatedData<T> prepareDataToReturn(List<T> list, CustomDiffCallback<T> cb, boolean detectMoves) {
        if (cb == null) {
            return new UpdatedData<>(Collections.unmodifiableList(list), null);
        }
        return new UpdatedData<>(Collections.unmodifiableList(list), DiffUtil.calculateDiff(cb, detectMoves));
    }
}
