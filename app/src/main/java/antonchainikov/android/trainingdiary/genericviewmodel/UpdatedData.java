package antonchainikov.android.trainingdiary.genericviewmodel;

import android.support.v7.util.DiffUtil;

import java.util.List;

public class UpdatedData<T> {
    private List<T> data;
    private DiffUtil.DiffResult difference;

    public UpdatedData(List<T> data, DiffUtil.DiffResult difference) {
        this.data = data;
        this.difference = difference;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public DiffUtil.DiffResult getDifference() {
        return difference;
    }

    public void setDifference(DiffUtil.DiffResult difference) {
        this.difference = difference;
    }
}
