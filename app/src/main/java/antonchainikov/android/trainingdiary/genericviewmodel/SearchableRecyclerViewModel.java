package antonchainikov.android.trainingdiary.genericviewmodel;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import antonchainikov.android.trainingdiary.diarydata.DataWrapper;

public abstract class SearchableRecyclerViewModel<T> extends BasicRecyclerViewModel<T> implements SearchableViewModel<T> {
    private final static String TAG = "SearchableRecyclerViewModel";

    private SearchCriteria mCriteria;
    private String lastQuery;

    @Override
    public void setSearchCriteria(SearchCriteria criteria) {
        mCriteria = criteria;
    }

    public void init(@NonNull DataWrapper<T> dataset) {
        super.init(dataset);
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean onSearchQueryChanged(String searchQuery) {
        int minSearchQueryLength = 3;

        if (searchQuery != null && !searchQuery.toLowerCase().equals(lastQuery)) {

            if ( searchQuery.length() < minSearchQueryLength &&
                    lastQuery != null &&
                    lastQuery.length() >= minSearchQueryLength ) {

                mDataset.getAll(this);

            } else if (searchQuery.length() >= minSearchQueryLength) {
                List<T> result = new ArrayList<>();

                if (mCriteria == null || mItemsList == null) {
                    lastQuery = searchQuery.toLowerCase();
                    return false;
                }

                for (T element : mItemsList) {
                    if (mCriteria.meetsCriteria(element, searchQuery)) {
                        result.add(element);
                    }
                }
                getUpdateAdapterTask().executeTask(new UpdatedData<T>(result, null));
            } else {
                lastQuery = searchQuery.toLowerCase();
                return false;
            }
            lastQuery = searchQuery.toLowerCase();
            return true;
        }
        lastQuery = searchQuery;
        return false;
    }

}
