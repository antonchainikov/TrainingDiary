package antonchainikov.android.trainingdiary.genericviewmodel;

public interface SearchCriteria<T> {
    boolean meetsCriteria(T o1, String query);
}