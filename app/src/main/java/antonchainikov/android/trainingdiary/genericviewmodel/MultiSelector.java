package antonchainikov.android.trainingdiary.genericviewmodel;

public interface MultiSelector {
    boolean isItemSelected(int pos);
    boolean isInSelectionState();
    void toggleSelection(int position);
}
