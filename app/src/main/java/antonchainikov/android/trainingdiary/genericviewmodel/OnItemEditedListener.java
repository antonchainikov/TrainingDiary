package antonchainikov.android.trainingdiary.genericviewmodel;


import java.io.Serializable;

public interface OnItemEditedListener<T> extends Serializable{
    void onItemEdited(T oldItem, T newItem);
}
