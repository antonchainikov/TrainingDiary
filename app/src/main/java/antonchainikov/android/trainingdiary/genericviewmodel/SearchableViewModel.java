package antonchainikov.android.trainingdiary.genericviewmodel;

public interface SearchableViewModel<T> extends RecyclerViewModel<T> {
    boolean onSearchQueryChanged(String searchQuery);
    void setSearchCriteria(SearchCriteria criteria);
}
