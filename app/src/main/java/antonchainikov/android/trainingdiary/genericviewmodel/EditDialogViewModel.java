package antonchainikov.android.trainingdiary.genericviewmodel;

import android.arch.lifecycle.ViewModel;

import antonchainikov.android.trainingdiary.LiveTask;
import antonchainikov.android.trainingdiary.diarydata.UniqueID;

public class EditDialogViewModel<T extends UniqueID> extends ViewModel {

    private T mItem;
    private OnItemEditedListener<T> mItemEditedListener;

    private LiveTask mDismissTask;

    public void init() {
        mDismissTask = new LiveTask();
    }

    public void setItem(T item) {
        mItem = item;
    }

    public T getItem() {
        return mItem;
    }

    public void onPositiveButtonClick(T item) {
        if (mItem != null) {
            item.changeId(mItem.getId());
        }
        mDismissTask.executeTask();
        mItemEditedListener.onItemEdited(mItem, item);
    }

    public void onNegativeButtonClick() {
        mDismissTask.executeTask();
    }

    public LiveTask getDismissTask() {
        return mDismissTask;
    }

    public void setItemEditedListener(OnItemEditedListener<T> listener) {
        mItemEditedListener = listener;
    }


}
