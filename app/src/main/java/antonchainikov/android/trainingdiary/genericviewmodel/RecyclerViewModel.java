package antonchainikov.android.trainingdiary.genericviewmodel;

import antonchainikov.android.trainingdiary.LiveTask;



public interface RecyclerViewModel<T> extends OnItemEditedListener<T> {

    LiveTask<T> getOpenEditDialogTask();

    LiveTask getOpenAddDialogTask();

    LiveTask<T> getOpenAmountPickerDialogTask();

    LiveTask<UpdatedData<T>> getUpdateAdapterTask();

    LiveTask<Boolean> getChangeDeleteButtonVisibilityTask();

    LiveTask<Boolean> getChangeEditButtonVisibilityTask();

    LiveTask<Integer> getRedrawItemTask();

    void onRecyclerViewItemClick(int position);

    boolean onRecyclerViewItemLongClick(int position);

    boolean isItemSelected(int position);

    void onDeleteItemsClick();

    void onEditItemClick();

    void onAddItemClick();

    void onActionModeCanceled();
}




