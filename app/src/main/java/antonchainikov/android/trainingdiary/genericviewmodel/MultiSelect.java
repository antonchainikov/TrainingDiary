package antonchainikov.android.trainingdiary.genericviewmodel;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import antonchainikov.android.trainingdiary.LiveTask;

class MultiSelect<T> implements MultiSelector {

    private final static String TAG = "MultiSelector";

    interface SelectionState {
        SelectionState changeState(int selectedItemsCount);
        void updateMenu();
    }

    private LiveTask<Boolean> mSetElementsMenuVisibleTask;
    private LiveTask<Boolean> mSetEditButtonVisibleTask;
    private LiveTask<Integer> mRedrawItemTask;

    private Set<Integer> selectedItems = new HashSet<>();
    private SelectionState mState;

    public  static <T> MultiSelect<T> getInstance() {
        return new MultiSelect<>();
    }

    private MultiSelect() {
        mSetElementsMenuVisibleTask = new LiveTask<>();
        mSetEditButtonVisibleTask = new LiveTask<>();
        mRedrawItemTask = new LiveTask<>();
        mState = new NoItemsSelected();
    }

    public LiveTask<Boolean> getChangeElementsMenuVisibilityTask() {
        return mSetElementsMenuVisibleTask;
    }

    public LiveTask<Boolean> getChangeEditButtonVisibilityTask() {
        return mSetEditButtonVisibleTask;
    }

    public LiveTask<Integer> getRedrawItemTask() {
        return mRedrawItemTask;
    }

    // if not selected, then set selected , else deselect
    @Override
    public void toggleSelection(int position) {
        if (!selectedItems.remove(position)) {
            selectedItems.add(position);
        }
        mRedrawItemTask.executeTask(position);
        updateState();
    }

    @Override
    public boolean isInSelectionState() {
        return selectedItems.size() > 0;
    }

    boolean isItemEditAvailable() { return selectedItems.size() == 1; }

    @Override
    public boolean isItemSelected(int position) {
        return selectedItems.contains(position);
    }

    void clear() {
        Integer[] items = new Integer[selectedItems.size()];
        items = selectedItems.toArray(items);
        for (Integer position : items) {
            mRedrawItemTask.executeTask(position);
        }
        selectedItems.clear();
        updateState();
    }

    Iterator<T> getSelectedItems(List<T> itemsList) {
        List<T> items = new ArrayList<>(selectedItems.size());
        for (Integer pos : selectedItems) {
            items.add(itemsList.get(pos));
        }
        return items.iterator();
    }

    T extractSelectedItem(List<T> itemsList) {
        Log.d(TAG, selectedItems + " items selected");
        T item = null;
        Integer position;
        Iterator<Integer> items = selectedItems.iterator();
        if (items.hasNext()) {
            position = items.next();
            items.remove();
            if (position != null) {
                item = itemsList.get(position);
            }
            Log.d(TAG, selectedItems + " items selected");
            updateState();
        }
        return item;
    }

    private void updateState() {
        mState = mState.changeState(selectedItems.size());
        mState.updateMenu();
    }

    private class NoItemsSelected implements SelectionState {
        @Override
        public SelectionState changeState(int selectedItemsCount) {
            if (selectedItemsCount == 0) {
                return this;
            }
            if (selectedItemsCount != 1) {
                throw new IllegalStateException(selectedItemsCount + " items selected. Only 1 item should be selected");
            }
            return new FirstItemSelected();
        }

        @Override
        public void updateMenu() {
            mSetElementsMenuVisibleTask.executeTask(false);
        }
    }

    private class FirstItemSelected implements SelectionState {
        @Override
        public SelectionState changeState(int selectedItemsCount) {
            if (selectedItemsCount == 0 ) {
                return new NoItemsSelected();
            } else if (selectedItemsCount == 2 ) {
                return new TwoOrMoreItemsSelected();
            }
            throw new IllegalStateException(selectedItemsCount + " items selected. Only 0 or 2 items should be selected");
        }
        @Override
        public void updateMenu() {
            mSetElementsMenuVisibleTask.executeTask(true);
            mSetEditButtonVisibleTask.executeTask(true);
        }
    }

    private class OneItemLeft implements SelectionState {
        @Override
        public SelectionState changeState(int selectedItemsCount) {
            if (selectedItemsCount == 0 ) {
                return new NoItemsSelected();
            } else if (selectedItemsCount == 2 ) {
                return new TwoOrMoreItemsSelected();
            }
            throw new IllegalStateException(selectedItemsCount + " items selected. Only 0 or 2 items should be selected");
        }
        @Override
        public void updateMenu() {
            mSetEditButtonVisibleTask.executeTask(true);
        }
    }

    private class TwoOrMoreItemsSelected implements SelectionState {
        @Override
        public SelectionState changeState(int selectedItemsCount) {
            if (selectedItemsCount == 1) {
                return new OneItemLeft();
            } else if (selectedItemsCount == 0) {
                return new NoItemsSelected();
            }
            return this;
        }

        @Override
        public void updateMenu() {
            boolean visible = mSetEditButtonVisibleTask.getValue() == null ? false : mSetEditButtonVisibleTask.getValue();
            if (visible) {
                mSetEditButtonVisibleTask.executeTask(false);
            }
        }
    }
}
