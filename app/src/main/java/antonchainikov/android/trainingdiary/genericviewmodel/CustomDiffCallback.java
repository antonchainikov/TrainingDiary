package antonchainikov.android.trainingdiary.genericviewmodel;

import android.support.v7.util.DiffUtil;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public abstract class CustomDiffCallback<T> extends DiffUtil.Callback {
    protected List<T> mOldList;
    protected List<T> mNewList;

    @SuppressWarnings("unchecked")
    public CustomDiffCallback() {
        mOldList = Collections.EMPTY_LIST;
        mNewList = Collections.EMPTY_LIST;
    }

    @SuppressWarnings("unchecked")
    public void setNewList(final List<T> newList){
        mOldList = mNewList;
        if (newList == null || newList.size() == 0) {
            mNewList = Collections.EMPTY_LIST;
        } else {
            mNewList = new ArrayList<>(newList);
        }
    }

    @Override
    public int getOldListSize() {
        Log.i("CustomDiffCallback", "oldSize is " + mOldList.size());
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        Log.i("CustomDiffCallback", "newSize is " + mNewList.size());
        return mNewList.size();
    }

}
