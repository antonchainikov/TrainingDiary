package antonchainikov.android.trainingdiary.utils;


import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.EditText;

import antonchainikov.android.trainingdiary.R;
import antonchainikov.android.trainingdiary.menuforday.view.DietMasterFragment;

public class ViewUtils {

    public static boolean validateInput(String errMsg, EditText...input) {
        if (input != null) {
            for (EditText editText: input) {
                if (editText.getText().toString().equals("")) {
                    editText.setError(errMsg);
                    return false;
                }
            }
        }
        return true;
    }

    public static double getDoubleFromText(EditText editText) {
        return Double.parseDouble(editText.getText().toString().trim());
    }

    public static void setFragmentIntoContainer(FragmentManager fragmentManager, Fragment fragment, @IdRes int containerViewId) {
        fragmentManager.beginTransaction()
                .add(containerViewId, fragment)
                .commit();
    }
}
