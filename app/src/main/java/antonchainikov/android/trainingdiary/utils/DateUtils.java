package antonchainikov.android.trainingdiary.utils;


import java.util.Calendar;

public class DateUtils {
    //private final static String TAG = "DateUtils";
    public static long getCurrentDateWithoutTime(long milliseconds){
        Calendar cal = Calendar.getInstance();

        cal.setTimeInMillis(milliseconds);

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTimeInMillis();
    }
}
