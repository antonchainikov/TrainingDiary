package antonchainikov.android.trainingdiary.utils;


import android.content.Context;
import android.widget.TextView;

public class TextUtils {

    public static void updateTextViewData(Context context, TextView textView, int stringId, double data){
        textView.setText(format(context, stringId, data));
    }

    public static String format(Context context, int stringId, double data) {
        return context.getString( stringId , data );
    }


}
