package antonchainikov.android.trainingdiary.genericviewlayer;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import antonchainikov.android.trainingdiary.BR;
import antonchainikov.android.trainingdiary.genericviewmodel.RecyclerViewModel;
import antonchainikov.android.trainingdiary.genericviewmodel.UpdatedData;

public abstract class DiaryAdapter<T, V extends ViewDataBinding>
        extends RecyclerView.Adapter<DiaryAdapter.DiaryViewHolder<V>>
        {

    private RecyclerViewModel<T> mViewModel;
    private List<T> mItemsList;

    private final static String TAG = "DiaryAdapter";

    protected DiaryAdapter(@NonNull RecyclerViewModel<T> viewModel,
                           @NonNull LifecycleOwner lifecycleOwner) {
        mViewModel = viewModel;
        mViewModel.getUpdateAdapterTask().observe(lifecycleOwner, getUpdateAdapterTaskObserver());
    }

    @Override
    @NonNull
    public DiaryViewHolder<V> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(getLayoutToInflate(viewType), parent, false);

        final V binding = DataBindingUtil.bind(view);
        binding.setVariable(BR.viewModel, mViewModel);
        binding.executePendingBindings();

        return new DiaryViewHolder<>(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull DiaryViewHolder<V> holder, final int position) {
        final T item = mItemsList.get(position);
        ViewDataBinding binding = holder.getBinding();
        binding.setVariable(BR.item, item);
        binding.setVariable(BR.position, position);
        View itemView = binding.getRoot();
        itemView.setActivated(mViewModel.isItemSelected(position));
        binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mItemsList == null ? 0 : mItemsList.size();
    }

    protected abstract int getLayoutToInflate(int viewType);

    private Observer<UpdatedData<T>> getUpdateAdapterTaskObserver() {
        return new Observer<UpdatedData<T>>() {
            @Override
            public void onChanged(@Nullable UpdatedData<T> updatedData) {
                if (updatedData !=  null) {
                    mItemsList = updatedData.getData();
                    DiffUtil.DiffResult diffResult = updatedData.getDifference();
                    if (diffResult == null) {
                        notifyDataSetChanged();
                    } else {
                        diffResult.dispatchUpdatesTo(DiaryAdapter.this);
                    }
                }
            }
        };
    }


    static class  DiaryViewHolder<V extends ViewDataBinding> extends RecyclerView.ViewHolder {
        private V binding;
        DiaryViewHolder(V binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        V getBinding() {
            return binding;
        }
    }
}