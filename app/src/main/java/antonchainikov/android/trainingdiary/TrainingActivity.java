package antonchainikov.android.trainingdiary;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import antonchainikov.android.trainingdiary.diarydata.TrainingProgram;

/**
 *  Activity that contains Fragment with ongoing training procedure
 */

public class TrainingActivity extends SingleFragmentActivity {

    private final static String PROGRAM_ID_ARG =
            "antonchainikov.android.trainingdiary.TrainingActivity.programID";
    private final static long PROGRAM_NOT_FOUND = -1;

    public static Intent makeStartIntent(Context context, long programID) {
        Intent intent = new Intent(context, TrainingProgram.class);
        intent.putExtra(PROGRAM_ID_ARG, programID);
        return intent;
    }

    @Override
    protected Fragment createFragment() {
        long programId = getIntent().getLongExtra(PROGRAM_ID_ARG, PROGRAM_NOT_FOUND);
        return TrainingFragment.makeFragment(programId);
    }
}
