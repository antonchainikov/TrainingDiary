package antonchainikov.android.trainingdiary;


import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;

public class LiveTask<T> extends MutableLiveData<T> {
    private boolean mPendingTask = false;

    public LiveTask(){}

    public LiveTask(T initialValue) {
        setValue(initialValue);
    }

    @MainThread
    public void observe(@NonNull LifecycleOwner owner, @NonNull final Observer<T> observer) {
        super.observe(owner, new Observer<T>() {
            @Override
            public void onChanged(T t) {
                if (mPendingTask) {
                    mPendingTask = false;
                    observer.onChanged(t);
                }
            }
        });
    }

    @MainThread
    @Override
    public void setValue(T t) {
        mPendingTask = true;
        super.setValue(t);
    }

    @MainThread
    public void executeTask(T t) {
        setValue(t);
    }

    @MainThread
    public void executeTask() {
        setValue(null);
    }

}
