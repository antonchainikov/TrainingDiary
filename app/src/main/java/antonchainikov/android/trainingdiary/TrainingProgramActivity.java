package antonchainikov.android.trainingdiary;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import java.io.IOException;

import antonchainikov.android.trainingdiary.diarydata.DataWrapper;
import antonchainikov.android.trainingdiary.diarydata.DiaryDatabase;
import antonchainikov.android.trainingdiary.diarydata.TrainingProgram;


public class TrainingProgramActivity extends AbstractViewPagerActivity<TrainingProgram> {

    private static final String TAG = "TrainingProgramActivity";
    private static final long PROGRAM_ID_NOT_FOUND = -1;
    private static final String PROGRAM_ID_EXTRA =
            "antonchainikov.android.trainingdiary.programId";

    public static final int REQUEST_CODE_TRAINING_PLAN_DATA = 0;
    private long mProgramId;
    private FloatingActionButton mButton;

    private Bundle dataToAdd;


    public static Intent makeStartIntent(Context context, long programId){
        Intent startIntent = new Intent(context, TrainingProgramActivity.class);
        startIntent.putExtra(PROGRAM_ID_EXTRA, programId);
        return startIntent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate called " + System.currentTimeMillis());
        mProgramId = getIntent().getLongExtra(PROGRAM_ID_EXTRA, PROGRAM_ID_NOT_FOUND);
       /* mButton = (FloatingActionButton) findViewById(R.id.diet_fab);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Adding exercise for program id = " + mProgramId);
                Intent intent =
                        ExerciseListActivity.makeStartIntent(TrainingProgramActivity.this);
                startActivityForResult(intent, REQUEST_CODE_TRAINING_PLAN_DATA);
            }
        });*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TrainingProgramActivity.REQUEST_CODE_TRAINING_PLAN_DATA) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                dataToAdd = data.getBundleExtra(Intent.EXTRA_RETURN_RESULT);
            }
        }
    }

    @Override
    protected DataWrapper<TrainingProgram> getDatawrapper() {
        if (mProgramId == PROGRAM_ID_NOT_FOUND){
            Log.e(TAG, "Error! Program ID not found or invalid start argument");
            return null;
        }
        return DiaryDatabase.getInstance(this).getTrainingProgramDataset(this);
    }

    @Override
    protected Fragment getFragment(TrainingProgram programAtposition) {
        ExerciseListForTrainingProgramFragment fragment;
        if (mProgramId == programAtposition.getId()) {
            fragment = ExerciseListForTrainingProgramFragment
                    .makeNewFragment(programAtposition, dataToAdd);
            dataToAdd = null;
        } else {
            fragment = ExerciseListForTrainingProgramFragment
                    .makeNewFragment(programAtposition, null);
        }

        return fragment;
    }

    @Override
    protected void moveToChosenElement(ViewPager viewPager) {
        int itemIndex = 0;
        try {
           itemIndex = mDataset.getPosition(mProgramId);
        } catch (IOException e) {
            Log.e(TAG, "Error while setting focus on active item", e);
        }
        if (itemIndex >= 0) {
            viewPager.setCurrentItem(itemIndex, false);
        }
    }

}
