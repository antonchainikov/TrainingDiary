package antonchainikov.android.trainingdiary;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;


import java.io.IOException;

import antonchainikov.android.trainingdiary.diarydata.DataWrapper;


public abstract class AbstractViewPagerActivity<D> extends AppCompatActivity
        implements DataWrapper.OnCallbackListener
{
    private final static String TAG = "AbstractViewPagerAc-ty";

    protected DataWrapper<D> mDataset;
    private ViewPager mViewPager;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.diet_pager);
        initDataset();
        initViewPager();
    }

    @Override
    public void onPause(){
        super.onPause();
        if (mDataset != null){
            mDataset.close();
            mViewPager.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        if (mDataset != null){
            mDataset.update();
            Log.d(TAG, "onResume(), update() called");
        } else {
            initDataset();
        }
    }

    @CallSuper
    @Override
    public void onDataWrapperCallback() {
        setPagerAdapter();
        moveToChosenElement(mViewPager);
    }

    protected void moveToChosenElement(ViewPager viewPager) {
    }

    protected void setCurrentItem(int pos) {
        mViewPager.setCurrentItem(pos);
    }

    private void initDataset(){
        mDataset = getDatawrapper();
    }

    protected abstract DataWrapper<D> getDatawrapper();


    private void initViewPager(){
        mViewPager = (ViewPager) findViewById(R.id.diet_view_pager);
    }

    private void setPagerAdapter(){
        FragmentManager fm = getSupportFragmentManager();
        FragmentStatePagerAdapter adapter = new FragmentStatePagerAdapter(fm) {
            @Override
            public Fragment getItem(int position) {
                try {
                    return getFragment(mDataset.get(position));
                } catch (IOException e) {
                    Log.e(TAG, "Fragment with NULL argument created", e);
                    return getFragment(null);
                }
            }

            @Override
            public int getCount() {
                return mDataset.size();
            }
        };
        mViewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    protected abstract Fragment getFragment(D elementAtposition);


}
