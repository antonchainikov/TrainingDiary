package antonchainikov.android.trainingdiary;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import antonchainikov.android.trainingdiary.diarydata.Food;
import antonchainikov.android.trainingdiary.utils.TextUtils;


public class FoodHolder extends RecyclerView.ViewHolder {

    private TextView mNameView;
    private TextView mProteinView;
    private TextView mFatView;
    private TextView mCarbsView;
    private TextView mCaloriesView;
    private TextView mAmountTextView;
    private Food mFood;

    private Context mContext;



    public FoodHolder(Context context, LayoutInflater inflater, ViewGroup parent) {
        super(inflater.inflate(R.layout.list_item_food_legacy, parent, false));
        mContext = context;
        mNameView =     (TextView) itemView.findViewById(R.id.textView_Name1);
        mProteinView =  (TextView) itemView.findViewById(R.id.list_item_textView_Protein1);
        mFatView =      (TextView) itemView.findViewById(R.id.list_item_textView_Fat1);
        mCarbsView =    (TextView) itemView.findViewById(R.id.list_item_textView_Carbs1);
        mCaloriesView = (TextView) itemView.findViewById(R.id.textView_Calories1);
        mAmountTextView = (TextView) itemView.findViewById(R.id.list_item_textView_amount1);
    }

    public void bind(Food food){
        if (food != null) {
            mFood = food;
            mNameView.setText(mFood.getName());
        }
        updateMainViews();
    }

    public Food getFood(){
        return mFood;
    }

    private void updateMainViews(){
        TextUtils.updateTextViewData(
                mContext,mCaloriesView,
                R.string.nutrition_calories,
                mFood.getCalories());
        TextUtils.updateTextViewData(
                mContext,mProteinView,
                R.string.nutrition_protein,
                mFood.getProtein());
        TextUtils.updateTextViewData(
                mContext,mFatView,
                R.string.nutrition_fat,
                mFood.getFat());
        TextUtils.updateTextViewData(
                mContext,mCarbsView,
                R.string.nutrition_carbs,
                mFood.getCarbs());

        if ( mFood.getAmount() == -1 ) {
            mAmountTextView.setText("");
            mAmountTextView.setVisibility(View.GONE);
        }
        else {
            TextUtils.updateTextViewData(
                    mContext,mAmountTextView,
                    R.string.list_item_amount,
                    mFood.getAmount());
        }
    }
}
