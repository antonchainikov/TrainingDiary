package antonchainikov.android.trainingdiary.menuforday.viewmodel;

import java.util.Comparator;

import antonchainikov.android.trainingdiary.diarydata.Food;
import antonchainikov.android.trainingdiary.genericviewmodel.BasicRecyclerViewModel;

public class DietViewModel extends BasicRecyclerViewModel<Food> {






    @Override
    protected Comparator<Food> getComparator() {
        return Food.getComparator();
    }
}
