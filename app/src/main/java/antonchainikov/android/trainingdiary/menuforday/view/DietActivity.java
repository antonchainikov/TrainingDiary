package antonchainikov.android.trainingdiary.menuforday.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import antonchainikov.android.trainingdiary.AbstractViewPagerActivity;
import antonchainikov.android.trainingdiary.DatePickListener;
import antonchainikov.android.trainingdiary.DatePickerFragment;
import antonchainikov.android.trainingdiary.DietFragment;
import antonchainikov.android.trainingdiary.R;
import antonchainikov.android.trainingdiary.diarydata.DataWrapper;
import antonchainikov.android.trainingdiary.diarydata.DiaryDatabase;
import antonchainikov.android.trainingdiary.utils.DateUtils;

public class DietActivity extends AbstractViewPagerActivity<Long>
        implements DatePickListener, DataWrapper.OnCallbackListener
{

    private final static String DIALOG_TAG = "DatePickerDialog";
    private final static String TAG = "DietActivity";

    private FloatingActionButton mFloatingActionButton;

    public static Intent makeStartIntent(Context context){
        return new Intent(context, DietActivity.class);
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        initFloatingActionButton();
    }

    @Override
    public void onDatePicked(long millis) {
        addDate(millis);
    }

    @Override
    public void onDataWrapperCallback() {
        super.onDataWrapperCallback();
        addNewDietIfNeeded();
    }

    @Override
    protected DataWrapper<Long> getDatawrapper(){
        return DiaryDatabase.getInstance(this).getDietDataset(this);
    }

    private void initFloatingActionButton(){

    }

    @Override
    protected Fragment getFragment(Long element){
        return DietFragment.getInstance(element);
    }


    private void addNewDietIfNeeded(){
        if ( mDataset.size() == 0){
            long date = getCurrentDateInMilliseconds();
            addDate(date);
        }
    }

    private long getCurrentDateInMilliseconds() {
        return GregorianCalendar.getInstance().getTimeInMillis();
    }

    private void addDate(long date)  {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTimeInMillis(date);
        long dateNoTime = DateUtils.getCurrentDateWithoutTime(calendar.getTimeInMillis());
        try {
            mDataset.add(dateNoTime);
            mDataset.update();
        } catch (IOException e) {
            Log.e(TAG, "Error adding date", e);
        }

    }
}
