package antonchainikov.android.trainingdiary.menuforday.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import antonchainikov.android.trainingdiary.R;
import antonchainikov.android.trainingdiary.databinding.DietPagerBinding;

public class PagingFragment extends Fragment {

    private final static String TAG = "PagingFragment";

    private DietPagerBinding mBinding;

    public static Fragment getFragment() {
        return new PagingFragment();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle onSavedInstanceState) {
        mBinding = DietPagerBinding.inflate(inflater, container, false);
        initViewPager();
        return mBinding.getRoot();
    }

    private void initViewPager() {
        FragmentManager fm = getFragmentManager();
        FragmentStatePagerAdapter adapter = new FragmentStatePagerAdapter(fm) {
            @Override
            public Fragment getItem(int position) {
                    return FrameFragment.getFragment();
            }

            @Override
            public int getCount() {
                return 2;
            }
        };
        mBinding.dietViewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

}
