package antonchainikov.android.trainingdiary.menuforday.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import antonchainikov.android.trainingdiary.SingleFragmentActivity;
import antonchainikov.android.trainingdiary.menuforday.viewmodel.DietViewModel;


public class DietForDayActivity extends SingleFragmentActivity {

    DietViewModel mViewModel;

    public static Intent makeStartIntent(Context context){
        return new Intent(context, DietForDayActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(DietViewModel.class);
    }

    @Override
    protected Fragment createFragment() {
        return PagingFragment.getFragment();
    }



}
