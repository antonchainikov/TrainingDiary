package antonchainikov.android.trainingdiary.menuforday.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import antonchainikov.android.trainingdiary.R;
import antonchainikov.android.trainingdiary.databinding.DietMasterFragmentBinding;
import antonchainikov.android.trainingdiary.diarydata.Diet;

public class DietMasterFragment extends Fragment {

    private DietMasterFragmentBinding mBinding;

    public static Fragment getFragment() {
        return new DietMasterFragment();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle onSavedInstanceState) {
        mBinding = DietMasterFragmentBinding.inflate(inflater, container, false);
        mBinding.setDiet(Diet.getStub());
        return mBinding.getRoot();
    }
}
