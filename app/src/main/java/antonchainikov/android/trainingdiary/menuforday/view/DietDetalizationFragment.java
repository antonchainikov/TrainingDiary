package antonchainikov.android.trainingdiary.menuforday.view;

import android.arch.lifecycle.LifecycleOwner;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import antonchainikov.android.trainingdiary.R;
import antonchainikov.android.trainingdiary.databinding.DietDetailFragmentBinding;
import antonchainikov.android.trainingdiary.databinding.ListItemFoodBinding;
import antonchainikov.android.trainingdiary.diarydata.Food;
import antonchainikov.android.trainingdiary.genericviewlayer.DiaryAdapter;
import antonchainikov.android.trainingdiary.genericviewmodel.RecyclerViewModel;


public class DietDetalizationFragment extends Fragment {

    private DietDetailFragmentBinding mBinding;

    public static Fragment getFragment() {
        return new DietDetalizationFragment();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle onSavedInstanceState) {
        mBinding = DietDetailFragmentBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    private class DietAdapter extends DiaryAdapter<Food, ListItemFoodBinding> {

        protected DietAdapter(@NonNull RecyclerViewModel<Food> viewModel, @NonNull LifecycleOwner lifecycleOwner) {
            super(viewModel, lifecycleOwner);
        }

        @Override
        protected int getLayoutToInflate(int viewType) {
            return R.layout.list_item_food;
        }
    }
}
