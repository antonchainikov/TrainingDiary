package antonchainikov.android.trainingdiary.menuforday.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import antonchainikov.android.trainingdiary.R;
import antonchainikov.android.trainingdiary.utils.ViewUtils;

public class FrameFragment extends Fragment {

    private final static String TAG = "FrameFragment";

    public static Fragment getFragment() {
        return new FrameFragment();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle onSavedInstanceState) {
        View v = inflater.inflate(R.layout.menu_for_day, container, false);
        FragmentManager fragmentManager = getFragmentManager();
        Fragment masterFragment = fragmentManager.findFragmentById(R.id.dietforday_topframe);
        Fragment detalizationFragment = fragmentManager.findFragmentById(R.id.dietforday_bottomframe);
        Log.d(TAG, "Im here");
        if (masterFragment == null) {
            ViewUtils.setFragmentIntoContainer(fragmentManager, DietMasterFragment.getFragment(), R.id.dietforday_topframe);
        }
        if (detalizationFragment == null) {
            ViewUtils.setFragmentIntoContainer(fragmentManager, DietDetalizationFragment.getFragment(), R.id.dietforday_bottomframe);
        }
        return v;
    }
}
