package antonchainikov.android.trainingdiary;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;

import antonchainikov.android.trainingdiary.diarydata.DataWrapper;
import antonchainikov.android.trainingdiary.diarydata.DiaryDatabase;
import antonchainikov.android.trainingdiary.diarydata.Exercise;

/**
 * Fragment with ongoing Training routine
 */

public class TrainingFragment extends Fragment
        implements DataWrapper.OnCallbackListener {

    private final static String TAG = "TrainingFragment";

    private final static String PROGRAM_ID_ARG =
            "antonchainikov.android.trainingdiary.TrainingActivity.programID";
    private final static long PROGRAM_NOT_FOUND = -1;

    private DataWrapper<Exercise> mDataset;
    private long mProgramId;
    private RecyclerView mRecyclerView;

    public static TrainingFragment makeFragment(long programId) {
        TrainingFragment fragment = new TrainingFragment();
        Bundle args = new Bundle();
        args.putLong(PROGRAM_ID_ARG, programId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View fragmentView = inflater
                .inflate(R.layout.training_program_fragment, container, false);
        Bundle args = getArguments();
        if (args == null) {
            mProgramId = PROGRAM_NOT_FOUND;
        } else {
            mProgramId = args.getLong(PROGRAM_ID_ARG);
        }
        mDataset = DiaryDatabase.getInstance(getContext())
                .getExercisesForProgram(this, mProgramId);
        mRecyclerView = fragmentView.findViewById(R.id.exercise_list_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        return fragmentView;
    }

    @Override
    public void onDataWrapperCallback() {

    }

    private class TrainingAdapter extends RecyclerView.Adapter<TrainingViewHolder> {

        @Override
        public TrainingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return null;
        }

        @Override
        public void onBindViewHolder(TrainingViewHolder holder, int position) {
            Exercise exercise;
            try {
                exercise = mDataset.get(position);
            } catch (IOException e) {
                Log.e(TAG, "Error while reading data. Using stub instead", e);
                exercise = Exercise.getStub();
            }
            holder.bindView(exercise);
        }

        @Override
        public int getItemViewType(int position) {

            return 0;
        }

        @Override
        public int getItemCount() {
            return mDataset.size();
        }
    }

    private class TrainingViewHolder extends RecyclerView.ViewHolder {

        public TrainingViewHolder(View itemView) {
            super(itemView);
        }

        void bindView(Exercise exercise) {

        }
    }


}
