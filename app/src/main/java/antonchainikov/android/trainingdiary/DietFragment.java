package antonchainikov.android.trainingdiary;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.util.Date;

import antonchainikov.android.trainingdiary.diarydata.DataWrapper;
import antonchainikov.android.trainingdiary.diarydata.DiaryDatabase;
import antonchainikov.android.trainingdiary.diarydata.Diet;
import antonchainikov.android.trainingdiary.diarydata.Food;
import antonchainikov.android.trainingdiary.nutritionlist.view.NutritionListActivity;
import antonchainikov.android.trainingdiary.utils.TextUtils;


public class DietFragment extends Fragment implements DataWrapper.OnCallbackListener {

    private final static String TAG = "DietFragment";


    private final static int REQUEST_CODE_GET_FOOD_DATA = 0;

    private final static String ARGS_DIET =
            "antonchainikov.android.trainingdiary.DiaryData.Diet";

    private DataWrapper<Food> mFoodDataset;

    private RecyclerView mDietRecyclerView;
    private Button mAddButton;
    private TextView mCaloriesTextView;
    private TextView mProteinTextView;
    private TextView mFatTextView;
    private TextView mCarbsTextView;
    private TextView mCaptionTextView;

    private Diet mDiet;

    public static DietFragment getInstance(long date){

        DietFragment fragment = new DietFragment();
        Bundle args = new Bundle();
        args.putLong(ARGS_DIET, date);
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle onSavedInstanceState){

        View v = inflater.inflate(R.layout.diet_master_fragment, container, false);

        initRecyclerView(v);

        initAddButton(v);

        initNutritionTextViews(v);

        readArgs();

        return v;
    }

    @Override
    public void onResume(){
        super.onResume();
        if (mFoodDataset!= null){
            mFoodDataset.update();
        }
        mCaptionTextView.setText("n/a");
    }

    @Override
    public void onPause(){
        super.onPause();
        if (mFoodDataset!= null){
            mFoodDataset.close();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data ){
        if ( resultCode == Activity.RESULT_OK && data != null ){
            switch (requestCode){
                case REQUEST_CODE_GET_FOOD_DATA:{
                    Food food = data.getParcelableExtra(Intent.EXTRA_RETURN_RESULT);
                    addToMenu(food);
                    break;
                }
            }
        }
    }

    @Override
    public void onDataWrapperCallback() {
        initDiet();
        initRecyclerViewAdapter();
        updateViews();
    }

    private void initAddButton(View parent){
        mAddButton = parent.findViewById(R.id.diet_add_button);
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(
                        NutritionListActivity.makeStartIntent(getActivity()),
                        REQUEST_CODE_GET_FOOD_DATA
                );
            }
        });
    }

    private void initDiet(){
        DataWrapper.DietFactory factory = (DataWrapper.DietFactory) mFoodDataset;
        mDiet = factory.getDiet();
    }

    private void initRecyclerView(View parent){
        //mDietRecyclerView = parent.findViewById(R.id.diet_recyclerView);
        mDietRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void initRecyclerViewAdapter(){
        mDietRecyclerView.setAdapter(new MenuListAdapter());
    }

    private void initNutritionTextViews(View parent){

        mCaloriesTextView = parent.findViewById(R.id.diet_calories_textView);
        mProteinTextView = parent.findViewById(R.id.diet_protein_textView);
        mFatTextView = parent.findViewById(R.id.diet_fat_textView);
        mCarbsTextView = parent.findViewById(R.id.diet_carbs_textView);
        mCaptionTextView = parent.findViewById(R.id.diet_caption_textView);
    }

    private void addToMenu(Food food){
        try {
            mFoodDataset.add(food);
        } catch (IOException e) {
            Log.e(TAG, "Error while adding food", e);
        }
    }


    private void readArgs(){
        Bundle arguments = getArguments();
        if (arguments == null || arguments.isEmpty()){
            return;
        }
        long date = arguments.getLong(ARGS_DIET);
        mFoodDataset = DiaryDatabase.getInstance(getActivity()).getFoodDatasetForDay(this, date);
        updateViews();
    }

    private class MenuListHolder extends FoodHolder {

        MenuListHolder(Context context, LayoutInflater inflater, ViewGroup parent) {
            super(context, inflater, parent);
        }
    }

    private class MenuListAdapter extends RecyclerView.Adapter<MenuListHolder>{

        @Override
        public MenuListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            return new MenuListHolder(getActivity(), inflater, parent);
        }

        @Override
        public void onBindViewHolder(MenuListHolder holder, int position) {
            try {
                holder.bind( mFoodDataset.get(position) );
            } catch (IOException e){
                holder.bind(Food.getStub());
                Log.e(TAG, "Stub Food instance created", e);
            }

        }

        @Override
        public int getItemCount() {
            return mFoodDataset.size();
        }
    }

    private void updateViews(){
        if (mDiet == null) {
            return;
        }
        mCaptionTextView.setText(new Date(mDiet.getDate()).toString());
        Context context = getActivity();
        if ( context == null){
            return;
        }
        TextUtils.updateTextViewData(
                context,
                mCaloriesTextView,
                R.string.nutrition_calories,
                Math.round(mDiet.getCalories()));
        TextUtils.updateTextViewData(
                context,
                mProteinTextView,
                R.string.nutrition_protein,
                Math.round(mDiet.getProtein()));
        TextUtils.updateTextViewData(
                context,
                mFatTextView,
                R.string.nutrition_fat,
                Math.round(mDiet.getFat()));
        TextUtils.updateTextViewData(
                context,
                mCarbsTextView,
                R.string.nutrition_carbs,
                Math.round(mDiet.getCarbs()));
    }
}
