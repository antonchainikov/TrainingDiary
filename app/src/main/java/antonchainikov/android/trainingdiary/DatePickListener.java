package antonchainikov.android.trainingdiary;


public interface DatePickListener {

    public void onDatePicked(long millis);

}
