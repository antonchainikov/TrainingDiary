package antonchainikov.android.trainingdiary;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.IOException;

import antonchainikov.android.trainingdiary.diarydata.DataWrapper;
import antonchainikov.android.trainingdiary.diarydata.DiaryDatabase;
import antonchainikov.android.trainingdiary.diarydata.Exercise;

/**
 *  Contains list with all saved exercises to choose from
 */

public class ExerciseListFragment extends Fragment implements DataWrapper.OnCallbackListener {

    private final static String TAG = "ExerciseListFragment";

    private DataWrapper<Exercise> mDataset;
    private RecyclerView mRecyclerView;

    public static Fragment makeFragment() {
        return new ExerciseListFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle onSavedInstanceState) {
        View outputView =
                inflater.inflate(R.layout.exercise_list_fragment, container, false);

        mDataset = DiaryDatabase.getInstance(getContext()).getExerciseDataset(this);
        mRecyclerView = (RecyclerView) outputView.findViewById(R.id.exercise_list_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        return outputView;
    }


    @Override
    public void onDataWrapperCallback() {
        mRecyclerView.setAdapter(new RecyclerView.Adapter<PlainExerciseHolder>() {
            @Override
            public PlainExerciseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                LayoutInflater inflater = LayoutInflater.from(getContext());
                View view = inflater
                        .inflate(R.layout.list_item_exercise_plain, parent, false);
                return new PlainExerciseHolder(view);
            }

            @Override
            public void onBindViewHolder(PlainExerciseHolder holder, int position) {
                try {
                    holder.bindView(mDataset.get(position));
                } catch (IOException e) {
                    Log.e(TAG,"Error while reading exercise data, so using default values instead", e);
                    holder.bindView(Exercise.getStub());
                }
            }

            @Override
            public int getItemCount() {
                return mDataset.size();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mDataset != null) {
            mDataset.update();
        } else {
            mDataset = DiaryDatabase.getInstance(getContext()).getExerciseDataset(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mDataset != null) {
            mDataset.close();
        }
    }

    private class PlainExerciseHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private TextView mNameTextView;
        private TextView mDescrTextView;
        private Exercise mExercise;

        PlainExerciseHolder(View itemView) {
            super(itemView);
            mNameTextView = itemView.findViewById(R.id.exercise_item_header_name_textView);
            mDescrTextView = itemView.findViewById(R.id.exercise_item_descr_textView);
            itemView.setOnClickListener(this);
        }

        void bindView(Exercise exercise) {
            mExercise = (exercise == null)? Exercise.getStub() : exercise;
            mNameTextView.setText(mExercise.getName());
            mDescrTextView.setText(mExercise.getDescription());
        }

        @Override
        public void onClick(View v) {
            FragmentManager fragmentManager = getFragmentManager();
            TrainingPlanCreationDialog dialog =
                    TrainingPlanCreationDialog.getDialog(
                            mExercise.getName(),
                            mExercise.getDescription(),
                            mExercise.getId()
                    );
            dialog.show(fragmentManager, "TrainingPlanCreationDialog");
        }
    }
}
