package antonchainikov.android.trainingdiary;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.IOException;

import antonchainikov.android.trainingdiary.diarydata.DataWrapper;
import antonchainikov.android.trainingdiary.diarydata.DiaryDatabase;
import antonchainikov.android.trainingdiary.diarydata.TrainingProgram;

/**
 *  Fragment that shows Training programs list, has add & edit buttons
 */


public class TrainingListFragment extends Fragment implements DataWrapper.OnCallbackListener {

    private static final String TAG = "TrainingListFragment";

    private static final String STRING_STUB = "n/a";
    private static final int INT_STUB = 0;

    private Spinner mSpinner;
    private DataWrapper<TrainingProgram> mDataset;
    private Button mAddButton;
    private Button mEditButton;
    private Button mStartButton;
    private long mSelectedProgramId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.training_fragment, parent, false);
        mSpinner = v.findViewById(R.id.training_fragment_spinner);
        mAddButton = v.findViewById(R.id.training_add_button);
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                AddProgramDialog dialog = AddProgramDialog.createDialog();
                dialog.show(fragmentManager, TAG);
            }
        });
        mAddButton.setEnabled(false);
        mEditButton = v.findViewById(R.id.training_edit_button);
        mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelectedProgramId != -1) {
                    startActivity(TrainingProgramActivity.
                            makeStartIntent(getActivity(),
                                    mSelectedProgramId));
                }
            }
        });
        mStartButton = v.findViewById(R.id.training_start_button);
        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(TrainingActivity.makeStartIntent(getContext(), mSelectedProgramId));
            }
        });
        return v;
    }

    @Override
    public void onResume(){
        super.onResume();
        if (mDataset == null){
            mDataset = DiaryDatabase.getInstance(getActivity()).getTrainingProgramDataset(this);
        } else {
            mDataset.update();
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        mDataset.close();
    }



    @Override
    public void onDataWrapperCallback() {
        mSpinner.setAdapter(new BaseAdapter() {
            Context context = getActivity();
            @Override
            public int getCount() {
                return mDataset.size();
            }

            @Override
            public Object getItem(int position) {
                try {
                    return mDataset.get(position).getName();
                } catch (IOException e) {
                    Log.e(TAG, "Stub data used",e);
                    return STRING_STUB;
                }

            }

            @Override
            public long getItemId(int position) {
                try {
                    return mDataset.get(position).getId();
                } catch (IOException e){
                    Log.e(TAG, "Stub data used", e);
                    return INT_STUB;
                }

            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TrainingProgram program;
                try {
                    program = mDataset.get(position);
                } catch (IOException e){
                    Log.e(TAG, "Stub data used", e);
                    program = new TrainingProgram(STRING_STUB, STRING_STUB);
                }

                TextView outputView;
                if ( convertView != null ){
                    outputView = (TextView) convertView;
                } else {
                    outputView = (TextView)
                            LayoutInflater.from(context)
                                    .inflate(android.R.layout.simple_list_item_1, parent, false);
                }
                outputView.setText(program.getName());
                return outputView;
            }
        });
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSelectedProgramId = id;
                Log.d(TAG, "<<<<<< id is " + id + " position is "+ position + ">>>>>>>");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mSelectedProgramId = -1;
            }
        });
        mAddButton.setEnabled(true);
        if (mSpinner.getCount() > 0){
            mEditButton.setEnabled(true);
        }
    }
}
