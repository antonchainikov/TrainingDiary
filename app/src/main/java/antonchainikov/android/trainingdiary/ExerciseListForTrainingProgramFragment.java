package antonchainikov.android.trainingdiary;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.IOException;

import antonchainikov.android.trainingdiary.diarydata.DataWrapper;
import antonchainikov.android.trainingdiary.diarydata.DiaryDatabase;
import antonchainikov.android.trainingdiary.diarydata.Exercise;
import antonchainikov.android.trainingdiary.diarydata.TrainingProgram;


/**
 *  Displays exercises in a RecyclerView for one particular Training Program
 *  Its the bottom half of Training Program detalization
 */

public class ExerciseListForTrainingProgramFragment extends Fragment
        implements DataWrapper.OnCallbackListener {

    private static final String TAG = "ExerciseListForProgram";

    private static final String PROGRAM_ARGUMENT =
            "antonchainikov.android.trainingdiary.ExerciseListForTrainingProgramFragment.programIDargument";
    private static final String DATA_TO_ADD_ARGUMENT =
            "antonchainikov.android.trainingdiary.ExerciseListForTrainingProgramFragment.mDataToAdd";

    private RecyclerView mRecyclerView;
    private TextView mNameTextView;
    private TextView mDescriptionTextView;

    private TrainingProgram mTrainingProgram;
    private DataWrapper<Exercise> mDataset;

    private Bundle mDataToAdd;

    public static ExerciseListForTrainingProgramFragment makeNewFragment(
            TrainingProgram trainingProgram,
            Bundle dataToAdd) {

        ExerciseListForTrainingProgramFragment fragment = new ExerciseListForTrainingProgramFragment();
        Bundle args = new Bundle();
        args.putParcelable(PROGRAM_ARGUMENT, trainingProgram);
        fragment.setDataToAdd(dataToAdd);
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle onSavedInstanceState){
        Log.d(TAG, "onCreateView called " + System.currentTimeMillis());
        View outputView = inflater.inflate(R.layout.training_program_fragment, container, false);

        readArgs();

        initDataset();

        initRecyclerView(outputView);

        initProgramViews(outputView);

        return outputView;
    }

    @Override
    public void onDataWrapperCallback() {
        initRecyclerViewAdapter();
    }

    public void setDataToAdd(Bundle dataToAdd) {
        mDataToAdd = dataToAdd;
    }

    private void initProgramViews(View outputView){
        mNameTextView =
                outputView.findViewById(R.id.training_program_fragment_name_textView);
        mNameTextView.setText(mTrainingProgram.getName());

        mDescriptionTextView =
                outputView.findViewById(R.id.training_program_fragment_descr_textView);
        mDescriptionTextView.setText(mTrainingProgram.getDescription());
    }

    private void initRecyclerViewAdapter(){
        mRecyclerView.setAdapter(new ExerciseAdapter());
    }


    private void initDataset(){
        mDataset = DiaryDatabase
                .getInstance(getActivity())
                .getExercisesForProgram(this, mTrainingProgram.getId());

        if (mDataToAdd != null) {
            Exercise exercise = Exercise.fromBundle(mDataToAdd);
            mDataToAdd = null;
            try {
                mDataset.add(exercise);
            } catch (IOException e) {
                Log.e(TAG, "Failed to add new exercise " + exercise.getName(), e);
            }
        }
    }

    private void initRecyclerView(View outputView){
        mRecyclerView = outputView.findViewById(R.id.exercise_list_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void readArgs(){
        Bundle args = getArguments();
        if (args == null){
            Log.e(TAG, " Error! No arguments! ");
        } else {
            TrainingProgram trainingProgram = (TrainingProgram)
                    args.getParcelable(PROGRAM_ARGUMENT);
            if (trainingProgram == null) {
                Log.e(TAG, " Error! No arguments! ");
            } else {
                mTrainingProgram = trainingProgram;
            }
        }
    }

    private class ExerciseHolder extends RecyclerView.ViewHolder{

        private TextView mExerciseNameTextView;
        private TextView mWeightTextView;
        private TextView mRepsTextView;
        private TextView mSetsTextView;
        private TextView mTimeoutTextView;

        ExerciseHolder(View itemView) {
            super(itemView);
            mExerciseNameTextView = itemView.findViewById(R.id.exercise_item_header_name_textView);
            mWeightTextView = itemView.findViewById(R.id.exercise_item_weight_textView);
            mRepsTextView = itemView.findViewById(R.id.exercise_item_reps_textView);
            mSetsTextView = itemView.findViewById(R.id.exercise_item_sets_textView);
            mTimeoutTextView = itemView.findViewById(R.id.exercise_item_timeout_textView);
        }

        public void bindView(Exercise exercise) {
            Exercise.ExercisePlan plan = exercise.getExercisePlan();
            mExerciseNameTextView.setText(exercise.getName());
            mWeightTextView.setText(Double.toString(plan.getWeight()));
            mRepsTextView.setText(Integer.toString(plan.getReps()));
            mSetsTextView.setText(Integer.toString(plan.getSets()));
            mTimeoutTextView.setText(Double.toString(plan.getTimeout()));
        }
    }

    private class ExerciseAdapter extends RecyclerView.Adapter<ExerciseHolder>{
        @Override
        public ExerciseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View view = inflater.inflate(R.layout.list_item_exercise, parent, false);
            return new ExerciseHolder(view);
        }

        @Override
        public void onBindViewHolder(ExerciseHolder holder, int position) {
            try {
                holder.bindView(mDataset.get(position));
            } catch (IOException e) {
                Log.e(TAG, "Error while getting item, using stub instead", e);
                holder.bindView(Exercise.getStub());
            }

        }

        @Override
        public int getItemCount() {
            return mDataset.size();
        }
    }
}
