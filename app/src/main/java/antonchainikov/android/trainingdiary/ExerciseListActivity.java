package antonchainikov.android.trainingdiary;


import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

/**
 * Activity For holding Fragment with Exercises to add to the training programs
 */

public class ExerciseListActivity extends SingleFragmentActivity {

    public static Intent makeStartIntent(Context context) {
        Intent intent = new Intent(context, ExerciseListActivity.class);
        return intent;
    }

    @Override
    protected Fragment createFragment() {
        return ExerciseListFragment.makeFragment();
    }
}
