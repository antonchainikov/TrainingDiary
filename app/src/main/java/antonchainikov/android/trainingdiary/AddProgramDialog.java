package antonchainikov.android.trainingdiary;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import java.io.IOException;

import antonchainikov.android.trainingdiary.diarydata.DataWrapper;
import antonchainikov.android.trainingdiary.diarydata.DiaryDatabase;
import antonchainikov.android.trainingdiary.diarydata.TrainingProgram;

public class AddProgramDialog extends DialogFragment implements Dialog.OnClickListener{

    final static String TAG = "AddProgramDialog";

    private EditText mNameEditText;
    private EditText mDescriptionText;

    public static AddProgramDialog createDialog() {
        return new AddProgramDialog();
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View dialogView = LayoutInflater
                .from(getContext()).inflate(R.layout.add_training_program_dialog, null);
        mNameEditText = dialogView.findViewById(R.id.add_training_dialog_name_editText);
        mDescriptionText = dialogView.findViewById(R.id.add_training_dialog_description_editText);
        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.add_trainingprogram_dialog_title)
                .setView(dialogView)
                .setPositiveButton(android.R.string.ok, this)
                .setNegativeButton(android.R.string.cancel, null)
                .create();
    }

    private static boolean editTextIsEmpty(EditText editText) {
        return editText.getText().toString().trim().length() == 0;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        boolean emptyName = false;
        boolean emptyDescr = false;
        if (editTextIsEmpty(mNameEditText)) {
            emptyName = true;
        }
        if (editTextIsEmpty(mDescriptionText)) {
            emptyDescr = true;
        }
        if ( !( emptyName || emptyDescr ) ) {
            String name = mNameEditText.getText().toString().trim();
            String descr = mDescriptionText.getText().toString().trim();
            TrainingProgram trainingProgram = new TrainingProgram(name, descr);
            DataWrapper<TrainingProgram> dataWrapper = DiaryDatabase
                    .getInstance(getContext())
                    .getTrainingProgramDataset(null);
            try {
                long programId = dataWrapper.add(trainingProgram);
                startActivity(TrainingProgramActivity.makeStartIntent(getActivity(), programId));
            } catch (IOException e) {
                Log.e(TAG, "Skipping activity start", e);
            }
        }
    }

}
