package antonchainikov.android.trainingdiary.diarydata;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

class FoodDataset extends Dataset<Food> {

    private final static String TAG = "FoodDataset";
    private String mPatternToSearch;


    private SQLiteDatabase mDatabase;
    private CountDownLatch latch;

    FoodDataset(CustomAsyncLoader<Food> loader,
                SQLiteDatabase database,
                OnCallbackListener listener) {

        super(loader, listener);
        mDatabase = database;
        latch = new CountDownLatch(1);
    }

    @Override
    public Food get(int pos) throws IOException{
        if (mCursorWrapper == null || mCursorWrapper.isClosed()) {
            throw new IOException("Cursor is closed or haven't been initialized");
        }
        return mCursorWrapper.get(pos);
    }

    @Override
    public void getAll(final OnLoadCompleteListener<Food> listener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    latch.await();
                    int count = mCursorWrapper != null ? mCursorWrapper.getCount() : 0;
                    final List<Food> output = new ArrayList<>(count);
                    try {
                        for (int i = 0; i < count; i++) {
                            output.add(mCursorWrapper.get(i));
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "error");
                    }
                    new Handler(Looper.getMainLooper()).post(onLoadComplete(listener, output));
                } catch (InterruptedException e) {
                    Log.e(TAG, "Latch interrupted, failed to update the list");
                }
            }
        }).start();
    }

    private Runnable onLoadComplete(final OnLoadCompleteListener<Food> listener, final List<Food> output) {
        return new Runnable() {
            @Override
            public void run() {
                listener.onLoadComplete(output);
            }
        };
    }

    @Override
    public long add(final Food element) throws IOException {
        long rowId = -1;
        if ( element != null ) {
            if (element.getId() > 0) {
                rowId = mDatabase.update(
                        DietDBContract.NutritionTable.NUTRITION_TABLE_NAME,
                        element.getContentValues(),
                        DietDBContract.NutritionTable.Cols._ID + " = ? ",
                        new String[] {Long.toString(element.getId())}
                );
            } else {
                rowId = mDatabase.insert(
                        DietDBContract.NutritionTable.NUTRITION_TABLE_NAME,
                        null,
                        element.getContentValues()
                );
            }

        }
        if (rowId == -1) {
            throw new IOException(ADD_ERR_MSG);
        }
        return rowId;
    }

    @Override
    public void delete(long id) {
        String[] args = {Long.toString(id)};
        mDatabase.delete(
                DietDBContract.NutritionTable.NUTRITION_TABLE_NAME,
                DietDBContract.NutritionTable.Cols._ID + " = ?",
                args
        );
    }

    @Override
    public void delete(Food element) {
        if ( element != null ) {
            delete(element.getId());
        }
    }

    @Override
    public int getPosition(Food element) throws IOException {
        if ( element == null ) {
            return -1;
        }
        return getPosition(element.getId());
    };

    @Override
    public int getPosition(long id) throws IOException {
        return ((NutritionCursorWrapper)mCursorWrapper).getPosition(id);
    }

    @Override
    public void search(String data) {
        if (data != null && data.length() > 2) {
            String pattern = ("%"+data.trim() + "%").toUpperCase();
            if (!pattern.equals(mPatternToSearch)) {
                mPatternToSearch = pattern;
                update();
            }
        } else if (data == null && mPatternToSearch != null) {
            mPatternToSearch = null;
            update();
        }
    }

    @Override
    protected void onDataSetInitialized() {
        latch.countDown();
    }

    @Override
    NutritionCursorWrapper getInBackground(){
        Cursor cursor = null;
        if (mPatternToSearch == null || mPatternToSearch.isEmpty()) {
            if (mDatabase != null) {
                cursor = query(null, null);
            }
        } else {
            cursor = query(
                    DietDBContract.NutritionTable.Cols.SEARCH_COL_NAME + " like ? ",
                    mPatternToSearch.trim().toUpperCase()
            );
        }
        return new NutritionCursorWrapper(cursor);
    }

    private Cursor query(String selection, String argument) {
        String[] args = null;
        if (selection != null && argument != null) {
            args = new String[] {argument};
        }
        return mDatabase.query(
                DietDBContract.NutritionTable.NUTRITION_TABLE_NAME,
                null,
                selection,
                args,
                null,
                null,
                null);
    }



}
