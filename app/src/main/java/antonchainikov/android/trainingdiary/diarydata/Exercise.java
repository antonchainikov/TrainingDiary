package antonchainikov.android.trainingdiary.diarydata;

import android.content.ContentValues;
import android.os.Bundle;

public class Exercise {
    private String mName;
    private String mDescription;
    private long mId;


    private final static String ARG_NAME =
            "antonchainikov.android.trainingdiary.DiaryData.Exercise.argumentName";
    private final static String ARG_DESCR =
            "antonchainikov.android.trainingdiary.DiaryData.Exercise.argumentDescr";
    private final static String ARG_ID =
            "antonchainikov.android.trainingdiary.DiaryData.Exercise.argumentId";
    private final static String ARG_WEIGHT =
            "antonchainikov.android.trainingdiary.DiaryData.Exercise.argumentWeight";
    private final static String ARG_REPS =
            "antonchainikov.android.trainingdiary.DiaryData.Exercise.argumentReps";
    private final static String ARG_SETS =
            "antonchainikov.android.trainingdiary.DiaryData.Exercise.argumentSets";
    private final static String ARG_TIMEOUT =
            "antonchainikov.android.trainingdiary.DiaryData.Exercise.argumentTimeout";


    private final static String DEFAULT_TEXT = "n/a";
    private final static int DEFAULT_NUM_FIELD = 0;

    private static Exercise sStub;

    private ExercisePlan mExercisePlan;

    Exercise(String name){
        mName = name;
        mDescription = DEFAULT_TEXT;
        mId = DEFAULT_NUM_FIELD;
    }

    public static Exercise fromBundle(Bundle arguments) {
        if (arguments != null) {
            Exercise exercise = new Exercise(
                    arguments.getString(ARG_NAME),
                    arguments.getString(ARG_DESCR),
                    arguments.getLong(ARG_ID)
            );
            exercise.setExercisePlan(arguments);
            return exercise;
        }
        return getStub();
    }

    public Exercise(String name, String description, long id){
        this(name);
        mDescription = description;
        mId = id;
    }

    public static Exercise getStub() {
        if (sStub == null) {
            sStub = new Exercise(DEFAULT_TEXT, DEFAULT_TEXT, DEFAULT_NUM_FIELD);
        }
        return sStub;
    }



    public String getName() {
        if (mName == null) {
            return DEFAULT_TEXT;
        }
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getDescription() {
        if (mDescription == null) {
            return DEFAULT_TEXT;
        }
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public long getId() {
        if (mId < 0) {
            return DEFAULT_NUM_FIELD;
        }
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }


    public ExercisePlan getExercisePlan() {
        return mExercisePlan;
    }

    public void setExercisePlan(double weight, int reps, int sets, double timeout) {
        mExercisePlan = new ExercisePlan(weight, reps, sets, timeout);
    }

    public void setExercisePlan(Bundle argumentsBundle) {
        if (argumentsBundle != null && argumentsBundle.size() >= 4) {
            double weight = argumentsBundle.getDouble(ARG_WEIGHT);
            int reps = argumentsBundle.getInt(ARG_REPS);
            int sets = argumentsBundle.getInt(ARG_SETS);
            double timeout = argumentsBundle.getDouble(ARG_TIMEOUT);
            mExercisePlan = new ExercisePlan(weight, reps, sets, timeout);
        }
    }

    @Override
    public String toString() {
        return mName + " " + getExercisePlan();
    }

    public static Bundle createArgumentsBundle(
            String name,
            String descr,
            long id,
            double weight,
            int reps,
            int sets,
            double timeout) {

        Bundle argumentsBundle = new Bundle();
        argumentsBundle.putString(ARG_NAME, name);
        argumentsBundle.putString(ARG_DESCR, descr);
        argumentsBundle.putLong(ARG_ID, id);
        argumentsBundle.putDouble(ARG_WEIGHT, weight);
        argumentsBundle.putInt(ARG_REPS, reps);
        argumentsBundle.putInt(ARG_SETS, sets);
        argumentsBundle.putDouble(ARG_TIMEOUT, timeout);
        return argumentsBundle;
    }

    public static ContentValues getContentValues(Exercise exerciseArg) {
        Exercise exercise = exerciseArg;
        if (exercise == null) {
            exercise = getStub();
        }
        ContentValues contentValues = new ContentValues();
        ExercisePlan plan = exercise.getExercisePlan();
        contentValues.put(TrainingDBContract.ExercisesByProgram.Cols.EXERCISE_ID, exercise.getId());
        contentValues.put(TrainingDBContract.ExercisesByProgram.Cols.WEIGHT, plan.getWeight());
        contentValues.put(TrainingDBContract.ExercisesByProgram.Cols.REPETITIONS_COUNT, plan.getReps());
        contentValues.put(TrainingDBContract.ExercisesByProgram.Cols.SETS_COUNT, plan.getSets());
        contentValues.put(TrainingDBContract.ExercisesByProgram.Cols.TIMEOUT, plan.getTimeout());
        return contentValues;
    }


    public class ExercisePlan{
        private int mSets;
        private double mWeight;
        private int mReps;
        private double mTimeout;
        private int mHashCode;

        ExercisePlan(double weight, int reps, int sets, double timeout){
            mSets = sets;
            mWeight = weight;
            mReps = reps;
            mTimeout = timeout;
            mHashCode = calcHashCode();
        }

        public int getSets() {
            if (mSets < 0) {
                return DEFAULT_NUM_FIELD;
            }
            return mSets;
        }

        public double getWeight() {
            if (mWeight < 0) {
                return DEFAULT_NUM_FIELD;
            }
            return mWeight;
        }

        public int getReps() {
            if (mReps < 0) {
                return DEFAULT_NUM_FIELD;
            }
            return mReps;
        }

        public double getTimeout() {
            if (mTimeout < 0) {
                return DEFAULT_NUM_FIELD;
            }
            return mTimeout;
        }

        @Override
        public String toString() {
            return mWeight + " " + mReps + " " + mSets + " " + mTimeout;
        }

        @Override
        public boolean equals(Object o) {
            if ( this == o ) {
                return true;
            }
            if ( !( o instanceof ExercisePlan ) ) {
                return false;
            }
            ExercisePlan exercisePlan = (ExercisePlan) o;
            return exercisePlan.getReps() == mReps  &&
                   exercisePlan.getSets() == mSets  &&
                   exercisePlan.getTimeout() == mTimeout &&
                   exercisePlan.getWeight() == mWeight;
        }

        @Override
        public int hashCode() {
            return mHashCode;
        }

        private int calcHashCode() {
            long result = mReps;
            result = 31*result + mSets;
            result = 31*result + Double.valueOf(mTimeout).hashCode();
            return Long.valueOf(result).hashCode();
        }
    }

}
