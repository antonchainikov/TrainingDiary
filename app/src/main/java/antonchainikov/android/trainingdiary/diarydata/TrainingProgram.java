package antonchainikov.android.trainingdiary.diarydata;


import android.os.Parcel;
import android.os.Parcelable;

public class TrainingProgram implements Parcelable{

    private final static String DEFAULT_TEXT = "n/a";
    private final static int DEFAULT_NUM = 0;

    private long mID;
    private String mName;
    private String mDescription;

    public TrainingProgram(String name, String description){
        mName = name;
        mDescription = description;
    }

    public TrainingProgram(long id, String name, String description){
        mName = name;
        mID = id;
        mDescription = description;
    }

    private TrainingProgram(Parcel in) {
        mID = in.readLong();
        mName = in.readString();
        mDescription = in.readString();
    }

    public static final Creator<TrainingProgram> CREATOR = new Creator<TrainingProgram>() {
        @Override
        public TrainingProgram createFromParcel(Parcel in) {
            return new TrainingProgram(in);
        }

        @Override
        public TrainingProgram[] newArray(int size) {
            return new TrainingProgram[size];
        }
    };

    public long getId() {
        if (mID < 0) {
            return DEFAULT_NUM;
        }
        return mID;
    }

    public String getName() {
        if (mName == null) {
            return DEFAULT_TEXT;
        }
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getDescription() {
        if (mDescription == null) {
            return DEFAULT_TEXT;
        }
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mID);
        dest.writeString(mName);
        dest.writeString(mDescription);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if ( !( o instanceof TrainingProgram ) ) {
            return false;
        }
        return mName.equals(((TrainingProgram) o).getName()) &&
                mDescription.equals(((TrainingProgram) o).getDescription());
    }

    @Override
    public int hashCode() {
        long result = mName.hashCode();
        result = 31*result + mDescription.hashCode();
        return Long.valueOf(result).hashCode();
    }
}
