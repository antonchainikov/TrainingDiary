package antonchainikov.android.trainingdiary.diarydata;

import android.database.Cursor;
import android.util.Log;

import java.io.IOException;


class NutritionCursorWrapper extends CustomCursorWrapper<Food> {

    private final static String TAG = "NutritionCursorWrapper";

    private static int indexId;
    private static int indexName;
    private static int indexProtein;
    private static int indexFat;
    private static int indexCarbs;
    private static int indexCalories;

    NutritionCursorWrapper( Cursor cursor ) {
        super(cursor);
        if (cursor != null) {
            indexId = cursor.getColumnIndex( DietDBContract.NutritionTable.Cols._ID );
            indexName = cursor.getColumnIndex( DietDBContract.NutritionTable.Cols.NAME );
            indexProtein = cursor.getColumnIndex( DietDBContract.NutritionTable.Cols.PROTEIN );
            indexFat = cursor.getColumnIndex( DietDBContract.NutritionTable.Cols.FAT );
            indexCarbs = cursor.getColumnIndex( DietDBContract.NutritionTable.Cols.CARBS );
            indexCalories = cursor.getColumnIndex( DietDBContract.NutritionTable.Cols.CALORIES );
        } else {
            Log.e(TAG, NULL_CURSOR_ERR_MSG);
        }

    }

    @Override
    Food getItemToReturn() {
        Cursor cursor = getCursor();
        return new Food (
                cursor.getInt(indexId),
                cursor.getString(indexName),
                cursor.getDouble(indexProtein),
                cursor.getDouble(indexFat),
                cursor.getDouble(indexCarbs),
                cursor.getDouble(indexCalories)
        );
    }

    public int getPosition(long id) throws IOException {
        return getPosition(id, indexId);
    }
}
