package antonchainikov.android.trainingdiary.diarydata;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.IOException;

class ExercisesForProgramDataset extends Dataset<Exercise> {

    private SQLiteDatabase mDatabase;
    private long mProgramId;

    private final static String TAG = "ExercisesForProgramD-t";

    ExercisesForProgramDataset(
            CustomAsyncLoader<Exercise> loader,
            long programId,
            SQLiteDatabase database,
            OnCallbackListener listener) {

        super(loader, listener);
        mDatabase = database;
        mProgramId = programId;
    }

    @Override
    public Exercise get(int pos) throws IOException {
        if (mCursorWrapper == null || mCursorWrapper.isClosed()) {
            throw new IOException("Cursor is closed or haven't been initialized");
        }
        return mCursorWrapper.get(pos);
    }

    @Override
    public void getAll(OnLoadCompleteListener<Exercise> listener) {

    }


    @Override
    public long add(Exercise element) throws IOException {
        if (element != null) {
            ContentValues values = Exercise.getContentValues(element);
            if (values != null) {
                values.put(TrainingDBContract.ExercisesByProgram.Cols.PROGRAM_ID, mProgramId);
                Log.d(TAG, "put " + values.toString());
            }
            return mDatabase.insert(
                    TrainingDBContract.ExercisesByProgram.TABLE_NAME,
                    null,
                    values);
        }
        return -1;
    }

    @Override
    public void delete(long id) {
        //TODO implement delete method
    }

    @Override
    public void delete(Exercise element) {
        if (element != null) {
            delete(element.getId());
        }
    }

    @Override
    public int getPosition(Exercise element) throws IOException {
        if (element == null) {
            return -1;
        }
        return getPosition(element.getId());
    }

    @Override
    public int getPosition(long id) throws IOException {
        return ((ExerciseCursorWrapper)mCursorWrapper).getPosition(id);
    }

    @Override
    ExerciseCursorWrapper getInBackground() {
        Log.d(TAG, "Cursor for program_id " + mProgramId);
        boolean exerciseWithPlan = true;
        String[] args = {Long.toString(mProgramId)};
        Cursor cursor = null;
        if (mDatabase != null) {
            cursor = mDatabase.rawQuery(
                    TrainingDBContract.JOIN_EXERCISES_BY_PROGRAM_QUERY,
                    args);
        }
        return new ExerciseCursorWrapper(cursor, exerciseWithPlan);
    }
}
