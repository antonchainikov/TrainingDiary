package antonchainikov.android.trainingdiary.diarydata;


import android.database.Cursor;


class NutritionWithAmountCursorWrapper extends NutritionCursorWrapper {

    private static int indexAmount;

    NutritionWithAmountCursorWrapper(Cursor cursor) {
        super(cursor);
        if (cursor != null) {
            indexAmount = cursor.getColumnIndex(DietDBContract.DietTable.Cols.AMOUNT);
        }
    }

    @Override
    Food getItemToReturn() {
        Food food = super.getItemToReturn();
        food.setAmount(getCursor().getDouble(indexAmount));
        return food;
    }

}
