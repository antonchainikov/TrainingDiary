package antonchainikov.android.trainingdiary.diarydata;


import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Comparator;


public class Food implements Parcelable, UniqueID {


    private final static String DEFAULT_TEXT = "n/a";
    private final static int DEFAULT_NUM_VALUE = 0;
    private final static long NO_ID = -1;

    private final String mName;
    private final double mProtein;
    private final double mFat;
    private final double mCarbs;
    private final double mCalories;
    private double mAmount;
    private long mId;


    //////////////////////////////////////////////
    private static Food sParcelFood;
    //////////////////////////////////////////////

    private static Food mStub;


    public Food(long id,String name, double p, double f, double c, double cals){
        mName = name;
        mProtein = p;
        mFat = f;
        mCarbs = c;
        mCalories = cals;
        mId = id;
        mAmount = 0;
    }

    public Food(String name, double p, double f, double c, double cals){
        mName = name;
        mProtein = p;
        mFat = f;
        mCarbs = c;
        mCalories = cals;
        mId = NO_ID;
        mAmount = 0;
    }

    // stub object constructor
    private Food(){
        this(
                DEFAULT_NUM_VALUE,
                DEFAULT_TEXT,
                DEFAULT_NUM_VALUE,
                DEFAULT_NUM_VALUE,
                NO_ID,
                DEFAULT_NUM_VALUE
        );
    }

    public static Food getStub() {
        if (mStub == null) {
            mStub = new Food();
        }
        return mStub;
    }


    // not used right now but who knows
    private Food(Parcel parcel){
        mProtein = parcel.readDouble();
        mFat = parcel.readDouble();
        mCarbs = parcel.readDouble();
        mCalories = parcel.readDouble();
        mAmount = parcel.readDouble();
        mName = parcel.readString();
        mId = parcel.readLong();
    }

    @Override
    public long getId() {
        return mId;
    }

    @Override
    public void changeId(long id) {
        mId = id;
    }

    public String getName() {
        if ( mName == null || mName.isEmpty() ) {
            return DEFAULT_TEXT;
        }
        return mName;
    }

    public double getProtein() {
        if ( mProtein < 0 ) {
            return DEFAULT_NUM_VALUE;
        }
        return mProtein;
    }

    public double getFat() {
        if (  mFat < 0 ) {
            return DEFAULT_NUM_VALUE;
        }
        return mFat;
    }

    public double getCarbs() {
        if ( mCarbs < 0 ) {
            return DEFAULT_NUM_VALUE;
        }
        return mCarbs;
    }

    public double getCalories() {
        if ( mCalories < 0 ) {
            return DEFAULT_NUM_VALUE;
        }
        return mCalories;
    }

    public double getAmount() {
        if ( mAmount < 0 ) {
            return DEFAULT_NUM_VALUE;
        }
        return mAmount;
    }

    public void setAmount(double amount) {
        mAmount = amount;
    }

    public static Comparator<Food> getComparator() {
        return new NameComparator();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        sParcelFood = this;

        /*
        dest.writeDouble(mProtein);
        dest.writeDouble(mFat);
        dest.writeDouble(mCarbs);
        dest.writeDouble(mCalories);
        dest.writeDouble(mAmount);
        dest.writeString(mName);
        dest.writeLong(mId);
        */

    }

    /**
     * Stupid parcel. I'm cheating and just saving instance in a static field for the sake of
     * performance, 'cause i'm not using objects in an IPC anyway
     */
    public static final Parcelable.Creator<Food> CREATOR = new Parcelable.Creator<Food>(){

        @Override
        public Food createFromParcel(Parcel source) {
            Food food = sParcelFood;
            sParcelFood = null;
            return food;

            /*
            return new Food(source);
            */
        }

        @Override
        public Food[] newArray(int size) {
            return new Food[size];
        }
    };

    /**
     * Doesn't take ID into account as elements name may change which leads to
     * the representation of entirely different entity but ID stays the same
     * for ex. carrot to chicken, if there already exists a chicken with the same qualities,
     * then the elements are equal
     */
    @Override
    public boolean equals(Object o) {
        if ( this == o ) {
            return true;
        }
        if ( !( o instanceof Food ) ) {
            return false;
        }
        Food food = (Food) o;
        return mName.equals(food.getName()) &&
                Double.compare(mProtein, food.getProtein()) == 0 &&
                Double.compare(mFat, food.getFat()) == 0 &&
                Double.compare(mCarbs, food.getCarbs()) == 0 &&
                Double.compare(mCalories, food.getCalories()) == 0 &&
                Double.compare(mAmount, food.getAmount()) == 0;
    }

    @Override
    public int hashCode() {
        long result = mName.hashCode();
        int primeNumber = 31;
        result = primeNumber*result + Double.valueOf(mProtein).hashCode();
        result = primeNumber*result + Double.valueOf(mFat).hashCode();
        result = primeNumber*result + Double.valueOf(mCarbs).hashCode();
        result = primeNumber*result + Double.valueOf(mCalories).hashCode();
        result = primeNumber*result + Double.valueOf(mAmount).hashCode();
        return Long.valueOf(result).hashCode();
    }

    @Override
    public String toString() {
        return mName;
    }

    ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(DietDBContract.NutritionTable.Cols.NAME,     mName);
        values.put(DietDBContract.NutritionTable.Cols.PROTEIN,  mProtein);
        values.put(DietDBContract.NutritionTable.Cols.FAT,      mFat);
        values.put(DietDBContract.NutritionTable.Cols.CARBS,    mCarbs);
        values.put(DietDBContract.NutritionTable.Cols.CALORIES, mCalories);
        return values;
    }

    ContentValues getContentValuesFodDay(long date){
        if (mAmount == 0) return null;
        ContentValues values = new ContentValues();
        values.put(DietDBContract.DietTable.Cols.DATE, date);
        values.put(DietDBContract.DietTable.Cols.FOOD_ID, mId);
        values.put(DietDBContract.DietTable.Cols.AMOUNT, mAmount);
        return values;
    }

    private static class NameComparator implements Comparator<Food> {

        @Override
        public int compare(Food o1, Food o2) {
            return o1.mName.compareTo(o2.mName);
        }
    }
}
