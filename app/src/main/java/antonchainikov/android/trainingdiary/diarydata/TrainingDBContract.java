package antonchainikov.android.trainingdiary.diarydata;

import android.provider.BaseColumns;

class TrainingDBContract {

    static final String DATABASE_NAME = "training_database.db";
    static final int DATABASE_VERSION = 1;

    static final String JOIN_EXERCISES_BY_PROGRAM_QUERY = "select * from " +
            ExercisesByProgram.TABLE_NAME +
            " inner join " +
            ExerciseTable.TABLE_NAME +
            " on " +
            ExercisesByProgram.Cols.EXERCISE_ID +
            " = " +
            ExerciseTable.TABLE_NAME +"."+ExerciseTable.Cols._ID +
            " where " +
            ExercisesByProgram.Cols.PROGRAM_ID + " = ? ";

    class ExerciseTable {
        static final String TABLE_NAME = "table_exercise";
        static final String QUERY_CREATE_TABLE = "create table "+ TABLE_NAME + "( " +
                Cols._ID +" integer primary key, " +
                Cols.NAME + " text, " +
                Cols.DESCRIPTION + " text " +
                ")";

        class Cols implements BaseColumns{
            static final String NAME = "table_exercise_name";
            static final String DESCRIPTION = "table_exercise_descr";
        }
    }

    class ProgramTable {
        static final String TABLE_NAME = "table_program";
        static final String QUERY_CREATE_TABLE = "create table "+ TABLE_NAME + "( " +
                Cols._ID +" integer primary key, " +
                Cols.NAME + " text, " +
                Cols.DESCRIPTION + " text " +
                ")";

        class Cols implements BaseColumns{
            static final String NAME = "table_program_name";
            static final String DESCRIPTION = "table_program_descr";
        }
    }

    class ExercisesByProgram {
        static final String TABLE_NAME = "table_exercise_by_program";
        static final String QUERY_CREATE_TABLE = "create table "+ TABLE_NAME + "( " +
                Cols._ID +" integer primary key, " +
                Cols.PROGRAM_ID + " integer, " +
                Cols.EXERCISE_ID + " integer, " +
                Cols.SETS_COUNT + " integer, " +
                Cols.REPETITIONS_COUNT + " integer, " +
                Cols.WEIGHT + " integer, " +
                Cols.TIMEOUT + " integer " +
                ")";

        class Cols implements BaseColumns {
            static final String PROGRAM_ID = "program_id";
            static final String EXERCISE_ID = "exercise_id";
            static final String SETS_COUNT = "n_sets";
            static final String REPETITIONS_COUNT = "n_repetitions";
            static final String WEIGHT = "weight";
            static final String TIMEOUT = "timeout";
        }
    }
    // TODO add exercise stats per plan because it can be changed later
    // TODO in the table above, but the data should remain unchanged here
    class HistoryTable {
        static final String TABLE_NAME = "table_history";
        static final String QUERY_CREATE_TABLE = "create table "+ TABLE_NAME + "( " +
                Cols._ID +" integer primary key, " +
                Cols.DATE + " integer, " +
                Cols.PROGRAM_ID + " integer, " +
                Cols.EXERCISE_ID + " integer, " +
                Cols.ACTUAL_SETS_COUNT + " integer, " +
                Cols.ACTUAL_REPETITIONS_COUNT + " integer, " +
                Cols.ACTUAL_WEIGHT + " integer, " +
                Cols.TIMEOUT + " integer " +
                ")";

        class Cols implements BaseColumns {
            static final String DATE = "date";
            static final String PROGRAM_ID = "program_id";
            static final String EXERCISE_ID = "exercise_id";
            static final String ACTUAL_SETS_COUNT = "a_n_sets";
            static final String ACTUAL_REPETITIONS_COUNT = "a_n_repetitions";
            static final String ACTUAL_WEIGHT = "a_weight";
            static final String TIMEOUT = "a_timeout";
        }
    }

}
