package antonchainikov.android.trainingdiary.diarydata;

import android.database.Cursor;
import android.util.Log;

import java.io.IOException;

class TrainingCursorWrapper extends CustomCursorWrapper<TrainingProgram> {

    private final static String TAG = "TrainingCursorWrapper";

    private int mNameColumnIndex;
    private int mIdColumnIndex;
    private int mDescriptionColumnIndex;

    TrainingCursorWrapper(Cursor cursor) {
        super(cursor);
        if (cursor != null) {
            mNameColumnIndex =
                    cursor.getColumnIndex(TrainingDBContract.ProgramTable.Cols.NAME);
            mIdColumnIndex =
                    cursor.getColumnIndex(TrainingDBContract.ProgramTable.Cols._ID);
            mDescriptionColumnIndex =
                    cursor.getColumnIndex(TrainingDBContract.ProgramTable.Cols.DESCRIPTION);
        } else {
            Log.e(TAG, NULL_CURSOR_ERR_MSG);
        }

    }

    public int getPosition(long id) throws IOException {
        return getPosition(id, mIdColumnIndex);
    }

    @Override
    TrainingProgram getItemToReturn() {
        Cursor cursor = getCursor();
        return new TrainingProgram(
                cursor.getLong(mIdColumnIndex),
                cursor.getString(mNameColumnIndex),
                cursor.getString(mDescriptionColumnIndex)
        );
    }

}
