package antonchainikov.android.trainingdiary.diarydata;


import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

class CustomAsyncLoader<D> extends AsyncTaskLoader<CustomCursorWrapper<D>> {

    private Dataset<D> mDataset;

    CustomAsyncLoader(Context context) {
        super(context);
    }

    void onAttachToDataset(Dataset<D> dataset) {
        mDataset = dataset;
    }

    @Override
    public CustomCursorWrapper<D> loadInBackground() {
        if ( mDataset == null ) {
            return null;
        }
        return mDataset.getInBackground();
    }

}