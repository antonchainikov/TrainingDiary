package antonchainikov.android.trainingdiary.diarydata;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.IOException;

class TrainingProgramDataset extends Dataset<TrainingProgram> {

    private SQLiteDatabase mDatabase;

    TrainingProgramDataset(CustomAsyncLoader<TrainingProgram> loader,
                           SQLiteDatabase database,
                           OnCallbackListener listener){

        super(loader, listener);
        mDatabase = database;
    }


    @Override
    public TrainingProgram get(int pos) throws IOException{
        if (mCursorWrapper == null || mCursorWrapper.isClosed()) {
            throw new IOException("Cursor is closed or haven't been initialized");
        }
        return mCursorWrapper.get(pos);
    }

    @Override
    public void getAll(OnLoadCompleteListener<TrainingProgram> listener) {

    }


    @Override
    public long add(TrainingProgram element) throws IOException{
        long rowId = -1;
        if (element != null) {
            rowId = mDatabase.insert(TrainingDBContract.ProgramTable.TABLE_NAME,
                    null,
                    getContentValues(element)
            );
            update();
        }
        if (rowId == -1) {
            throw new IOException(ADD_ERR_MSG);
        }
        return rowId;
    }

    @Override
    public void delete(long id) {
        //TODO implement method
    }

    @Override
    public void delete(TrainingProgram element) {
        if (element != null) {
            delete(element.getId());
        }
    }

    @Override
    public int getPosition(TrainingProgram element) throws IOException {
        if ( element == null ) {
            return -1;
        }
        return getPosition(element.getId());
    }

    @Override
    public int getPosition(long id) throws IOException {
        return ((TrainingCursorWrapper)mCursorWrapper).getPosition(id);
    }


    @Override
    TrainingCursorWrapper getInBackground() {
        Cursor cursor = null;
        if (mDatabase != null) {
            cursor = mDatabase.query(
                    TrainingDBContract.ProgramTable.TABLE_NAME,
                    null,null,null,null,null,null
            );
        }
        return new TrainingCursorWrapper(cursor);
    }

    private static ContentValues getContentValues(TrainingProgram program){
        ContentValues values = new ContentValues();
        values.put(
                TrainingDBContract.ProgramTable.Cols.NAME,
                program.getName()
        );
        values.put(
                TrainingDBContract.ProgramTable.Cols.DESCRIPTION,
                program.getDescription()
        );
        return values;
    }
}
