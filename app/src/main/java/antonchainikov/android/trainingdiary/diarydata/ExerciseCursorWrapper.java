package antonchainikov.android.trainingdiary.diarydata;

import android.database.Cursor;
import android.util.Log;

import java.io.IOException;

class ExerciseCursorWrapper extends CustomCursorWrapper<Exercise> {

    private final static String TAG = "ExerciseCursorWrapper";

    private int nameIndex;
    private int descriptionIndex;
    private int idIndex;

    private int setsIndex;
    private int repsIndex;
    private int weightsIndex;
    private int timeoutIndex;

    private boolean exerciseWithPlan;

    ExerciseCursorWrapper(Cursor cursor, boolean exerciseWithPlan) {
        super(cursor);
        if (cursor != null) {
            nameIndex = cursor
                    .getColumnIndex(TrainingDBContract.ExerciseTable.Cols.NAME);
            descriptionIndex = cursor
                    .getColumnIndex(TrainingDBContract.ExerciseTable.Cols.DESCRIPTION);
            idIndex = cursor
                    .getColumnIndex(TrainingDBContract.ExerciseTable.Cols._ID);

            this.exerciseWithPlan = exerciseWithPlan;
            if (exerciseWithPlan){
                try {
                    setsIndex = cursor
                            .getColumnIndexOrThrow(TrainingDBContract.ExercisesByProgram.Cols.SETS_COUNT);
                    repsIndex = cursor
                            .getColumnIndexOrThrow(TrainingDBContract.ExercisesByProgram.Cols.REPETITIONS_COUNT);
                    weightsIndex = cursor
                            .getColumnIndexOrThrow(TrainingDBContract.ExercisesByProgram.Cols.WEIGHT);
                    timeoutIndex = cursor
                            .getColumnIndexOrThrow(TrainingDBContract.ExercisesByProgram.Cols.TIMEOUT);
                }  catch (IllegalArgumentException e) {
                    Log.e(TAG, "Exercise doesn't have a plan", e);
                    this.exerciseWithPlan = false;
                }

            }
        }
    }

    public int getPosition(long id) throws IOException {
        return getPosition(id, idIndex);
    }

    @Override
    Exercise getItemToReturn() {
        Exercise exercise = new Exercise(
                mCursor.getString(nameIndex),
                mCursor.getString(descriptionIndex),
                mCursor.getInt(idIndex)
        );
        if (exerciseWithPlan){
            exercise.setExercisePlan(
                    mCursor.getDouble(weightsIndex),
                    mCursor.getInt(repsIndex),
                    mCursor.getInt(setsIndex),
                    mCursor.getDouble(timeoutIndex)
            );
        }
        return exercise;
    }

}
