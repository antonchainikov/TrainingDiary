package antonchainikov.android.trainingdiary.diarydata;

import android.support.v4.content.Loader;
import android.util.Log;

abstract class Dataset<D>
        implements Loader.OnLoadCompleteListener<CustomCursorWrapper<D>>,
        DataWrapper<D> {

    private final static String TAG = "Dataset Superclass";
    final static String ADD_ERR_MSG = "Error while adding element";

    CustomCursorWrapper<D> mCursorWrapper;

    private CustomAsyncLoader<D> mLoader;
    private DataWrapper.OnCallbackListener mDatasetCallbackListener;
    private boolean initialized;

    Dataset(CustomAsyncLoader<D> loader, DataWrapper.OnCallbackListener listener) {
        mDatasetCallbackListener = listener;
        initialized = false;
        setupLoader(loader);
        update();
    }

    @Override
    public int size() {
        if ( mCursorWrapper == null || mCursorWrapper.isClosed() ) {
            return 0;
        }
        return mCursorWrapper.getCount();

    }

    @Override
    public void update() {
        if (mLoader != null) {
            mLoader.forceLoad();
        }
    }

    @Override
    public void close() {
        if (mCursorWrapper != null) {
            mCursorWrapper.close();
        }
        if (mLoader != null) {
            mLoader.cancelLoad();
        }
    }

    @Override
    public void search(String data) {
    }

    @Override
    public void onLoadComplete(Loader<CustomCursorWrapper<D>> loader,
                                CustomCursorWrapper<D> loadedCursor) {

        CustomCursorWrapper<D> oldCursorWrapper = mCursorWrapper;
        mCursorWrapper = loadedCursor;
        if (mDatasetCallbackListener != null) {
            mDatasetCallbackListener.onDataWrapperCallback();
        }
        if ( oldCursorWrapper != null && oldCursorWrapper != mCursorWrapper ) {
            oldCursorWrapper.close();
        }
        initialized = true;
        onDataSetInitialized();
    }

    abstract CustomCursorWrapper<D> getInBackground();

    private void setupLoader(CustomAsyncLoader< D > loader) {
        mLoader = loader;
        if (mLoader != null) {
            mLoader.registerListener(0,this);
            mLoader.onAttachToDataset(this);
        } else {
            Log.e(TAG, "Registered null loader. Something is wrong.");
        }
    }

    protected void onDataSetInitialized() {
    }
}
