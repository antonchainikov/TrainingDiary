package antonchainikov.android.trainingdiary.diarydata;


import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import antonchainikov.android.trainingdiary.R;

public class DiaryDatabase extends SQLiteOpenHelper {

    private static String TAG = "DiaryDatabase";

    private static volatile DiaryDatabase sInstance;
    private SQLiteDatabase mDatabase;
    private Context mContext;
    private boolean mInProcessOfFilling;

    private DiaryDatabase(Context context,
                          String name,
                          SQLiteDatabase.CursorFactory factory,
                          int version){
        super(context, name, factory, version);
        mContext = context.getApplicationContext();

        mInProcessOfFilling = false;
    }

    public static DiaryDatabase getInstance(Context context){
        if (sInstance == null) {
            synchronized (DiaryDatabase.class){
                if ( sInstance == null ) {
                    sInstance = new DiaryDatabase(
                            context,
                            DietDBContract.DATABASE_NAME,
                            null,
                            DietDBContract.DATABASE_VERSION);
                    sInstance.openDB();
                }
            }
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DietDBContract.NutritionTable.QUERY_CREATE_TABLE);
        db.execSQL(DietDBContract.DietTable.QUERY_CREATE_TABLE);
        db.execSQL(TrainingDBContract.ExerciseTable.QUERY_CREATE_TABLE);
        db.execSQL(TrainingDBContract.ProgramTable.QUERY_CREATE_TABLE);
        db.execSQL(TrainingDBContract.ExercisesByProgram.QUERY_CREATE_TABLE);
        db.execSQL(TrainingDBContract.HistoryTable.QUERY_CREATE_TABLE);

        fillNutritionTable(db);
        fillExerciseTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public DataWrapper<Food> getFoodDataset(DataWrapper.OnCallbackListener listener){
        return new FoodDataset(new CustomAsyncLoader<Food>(mContext), mDatabase, listener);
    }

    public DataWrapper<Long> getDietDataset(DataWrapper.OnCallbackListener listener){
        return  new DateDataset(new CustomAsyncLoader<Long>(mContext), mDatabase, listener);
    }

    public DataWrapper<Food> getFoodDatasetForDay(DataWrapper.OnCallbackListener listener, long date){
        return new DietDataset(new CustomAsyncLoader<Food>(mContext), mDatabase, listener, date);
    }

    public DataWrapper<Exercise> getExerciseDataset(DataWrapper.OnCallbackListener listener){
        return new ExerciseDataset(new CustomAsyncLoader<Exercise>(mContext), mDatabase, listener);
    }

    public DataWrapper<Exercise> getExercisesForProgram(
            DataWrapper.OnCallbackListener listener,
            long programId){
        return new ExercisesForProgramDataset(
                new CustomAsyncLoader<Exercise>(mContext),
                programId,
                mDatabase,
                listener
        );
    }

    public DataWrapper<TrainingProgram> getTrainingProgramDataset(
            DataWrapper.OnCallbackListener listener){
        return new TrainingProgramDataset(
                new CustomAsyncLoader<TrainingProgram>(mContext),
                mDatabase,
                listener
        );
    }

    boolean isFillingTable(){
        return mInProcessOfFilling;
    }


    private void openDB(){
        mDatabase = getWritableDatabase();
    }


    private void fillNutritionTable(final SQLiteDatabase db) {
        mInProcessOfFilling = true;
        new Thread(new Runnable(){
            public void run(){
                InputStream is = mContext.getResources().openRawResource(R.raw.nutrition_raw);
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(is)
                );
                try {
                    String line;
                    while ( (line = reader.readLine()) != null ){
                        ContentValues values = getNutritionContentValues(line);
                        db.insert(DietDBContract.NutritionTable.NUTRITION_TABLE_NAME, null, values);
                    }
                } catch (IOException e){
                    Log.e(TAG, "Error while filling Nutrition table", e);
                }
                mInProcessOfFilling = false;
            }
        }).start();
    }

    private void fillExerciseTable(final SQLiteDatabase db){
        InputStream is = mContext.getResources().openRawResource(R.raw.exercises_raw);
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(is)
        );
        try {
            String line;
            while ( (line = reader.readLine()) != null ){
                ContentValues values = getExerciseContentValues(line);
                if (values == null){
                    continue;
                }
                db.insert(TrainingDBContract.ExerciseTable.TABLE_NAME, null, values);
            }
            Log.i(TAG, "<<<<<<<< Exercise Table Filled >>>>>>>>>>");
        } catch (IOException e){
            Log.e(TAG, "Error while filling Nutrition table", e);
        }
    }

    private static ContentValues getNutritionContentValues(String line){
        String columnSeparatorInFile = ",";
        ContentValues values = new ContentValues();
        String[] fields = line.split(columnSeparatorInFile);
        values.put(DietDBContract.NutritionTable.Cols.NAME,     fields[0].trim());
        values.put(DietDBContract.NutritionTable.Cols.PROTEIN,  fields[1]);
        values.put(DietDBContract.NutritionTable.Cols.FAT,      fields[2]);
        values.put(DietDBContract.NutritionTable.Cols.CARBS,    fields[3]);
        values.put(DietDBContract.NutritionTable.Cols.CALORIES, fields[4]);
        values.put(DietDBContract.NutritionTable.Cols.SEARCH_COL_NAME, fields[0].trim().toUpperCase());
        return values;
    }

    private ContentValues getExerciseContentValues(String line){
        String columnSeparatorInFile = ";";
        ContentValues values = new ContentValues();
        String[] fields = line.split(columnSeparatorInFile);
        if (fields.length < 2){
            return null;
        }
        values.put(TrainingDBContract.ExerciseTable.Cols.NAME,         fields[0].trim());
        values.put(TrainingDBContract.ExerciseTable.Cols.DESCRIPTION,  fields[1].trim());
        return values;
    }

}
