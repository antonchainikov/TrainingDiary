package antonchainikov.android.trainingdiary.diarydata;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.IOException;

class DietDataset extends Dataset<Food> implements DataWrapper.DietFactory {

    private final static String TAG = "DietDataset";

    private SQLiteDatabase mDatabase;
    private long mDate;
    private Diet mDiet;

    DietDataset(CustomAsyncLoader< Food > loader,
                SQLiteDatabase database,
                OnCallbackListener listener,
                long date){

        super(loader, listener);
        mDatabase = database;
        mDate = date;
    }

    @Override
    public Diet getDiet(){
        if (mDiet == null) {
            return new Diet(mDate);
        }
        return mDiet;
    }

    @Override
    public Food get(int pos) throws IOException{
        if (mCursorWrapper == null || mCursorWrapper.isClosed()) {
            throw new IOException("Cursor is closed or haven't been initialized");
        }
        return mCursorWrapper.get(pos);
    }

    @Override
    public void getAll(OnLoadCompleteListener<Food> listener) {

    }


    @Override
    public long add(Food element) throws IOException {
        long rowId = -1;
        if (element != null) {
            ContentValues values = element.getContentValuesFodDay(mDate);
            if (values == null) {
                Log.d(TAG, element.getName() + " : addition skipped because amount is 0");
            }
            rowId = mDatabase.insert(
                    DietDBContract.DietTable.DIET_TABLE_NAME,
                    null,
                    values
            );
        }
        if (rowId == -1) {
            throw new IOException(ADD_ERR_MSG);
        }
        return rowId;
    }

    @Override
    public void delete(long id) {
        String[] args = {
                Long.toString(mDate),
                Long.toString(id)
        };
        mDatabase.delete(
                DietDBContract.DietTable.DIET_TABLE_NAME,
                DietDBContract.DietTable.Cols.DATE + " = ? and " +
                DietDBContract.DietTable.Cols.FOOD_ID + " = ? " ,
                args
        );
    }

    @Override
    public void delete(Food element) {
        delete(element.getId());
    }

    @Override
    public int getPosition(Food element) throws IOException {
        if (element == null) {
            return -1;
        }
        return getPosition(element.getId());
    }

    @Override
    public int getPosition(long id) throws IOException {
        return ((NutritionCursorWrapper)mCursorWrapper).getPosition(id);
    }

    @Override
    NutritionWithAmountCursorWrapper getInBackground(){
        Cursor cursor = null;
        if (mDatabase != null) {
            cursor = getDietDataForDay(mDate);
        }
        NutritionWithAmountCursorWrapper output =
                new NutritionWithAmountCursorWrapper(cursor);
        updateDiet(output);
        return output;
    }

    private Cursor getDietDataForDay(long date){
        String[] dateSelection = new String[] {Long.toString(date)};
        return mDatabase.rawQuery(
                DietDBContract.QUERY_JOIN_ID,
                dateSelection
        );
    }

    private void updateDiet(NutritionWithAmountCursorWrapper cursor) {
        try {
            mDiet = new Diet(mDate, cursor);
        } catch (IOException e){
            mDiet = new Diet(mDate);
            Log.e(TAG, "Failed to properly initialize Diet, fallen back to default", e);
        }
    }
}
