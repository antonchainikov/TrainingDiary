package antonchainikov.android.trainingdiary.diarydata;


public interface UniqueID {
    long getId();
    void changeId(long id);
}
