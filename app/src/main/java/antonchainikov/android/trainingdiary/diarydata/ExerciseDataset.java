package antonchainikov.android.trainingdiary.diarydata;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.IOException;

class ExerciseDataset extends Dataset<Exercise> {

    private static final String TAG = "ExerciseDataset";

    private SQLiteDatabase mDatabase;

    ExerciseDataset(CustomAsyncLoader<Exercise> loader,
                    SQLiteDatabase database,
                    OnCallbackListener listener) {

        super(loader, listener);
        mDatabase = database;
    }

    @Override
    public Exercise get(int pos) throws IOException {
        if (mCursorWrapper == null || mCursorWrapper.isClosed()) {
            throw new IOException("Cursor is closed or haven't been initialized");
        }
        return mCursorWrapper.get(pos);
    }

    @Override
    public void getAll(OnLoadCompleteListener<Exercise> listener) {

    }


    @Override
    public long add(Exercise element) throws IOException{
        // TODO implement add method
        return 0;
    }

    @Override
    public void delete(long id) {
        // TODO implement delete method
    }

    @Override
    public void delete(Exercise element) {
        if (element != null) {
            delete(element.getId());
        }
    }

    @Override
    public int getPosition(Exercise element) throws IOException {
        if (element == null) {
            return -1;
        }
        return getPosition(element.getId());
    }

    @Override
    public int getPosition(long id) throws IOException {
        return ((ExerciseCursorWrapper)mCursorWrapper).getPosition(id);
    }

    @Override
    ExerciseCursorWrapper getInBackground() {
        boolean exerciseWithPlan = false;
        Cursor cursor = null;
        if (mDatabase != null) {
            cursor = mDatabase.query(
                    TrainingDBContract.ExerciseTable.TABLE_NAME,
                    null,null,null,null,null,null);
        }
        return new ExerciseCursorWrapper(cursor, exerciseWithPlan);
    }
}
