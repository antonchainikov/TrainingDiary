package antonchainikov.android.trainingdiary.diarydata;

import android.database.Cursor;
import android.util.Log;

import java.io.IOException;


class DateCursorWrapper extends CustomCursorWrapper<Long> {

    private final static String TAG = "DateCursorWrapper";
    private static int indexDate;

    static DateCursorWrapper makeFromCursor(Cursor cursor){
        return new DateCursorWrapper(cursor);
    }

    private DateCursorWrapper(Cursor cursor) {
        super(cursor);
        if (cursor != null) {
            indexDate = cursor.getColumnIndex(DietDBContract.DietTable.Cols.DATE);
        } else {
            indexDate = 0;
            Log.e(TAG, NULL_CURSOR_ERR_MSG);
        }
    }

    public int getPosition(long id) throws IOException {
        return getPosition(id, indexDate);
    }

    @Override
    Long getItemToReturn() {
        return getCursor().getLong(indexDate);
    }


}
