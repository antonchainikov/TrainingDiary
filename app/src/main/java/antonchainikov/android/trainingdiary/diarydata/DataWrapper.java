package antonchainikov.android.trainingdiary.diarydata;


import java.io.IOException;
import java.util.List;


public interface DataWrapper<D> extends AutoCloseable{

    D get(int pos) throws IOException;

    void getAll(OnLoadCompleteListener<D> listener);

    long add(D element) throws IOException;

    void delete(long id);

    void delete(D element);

    int size();

    void update();

    void close();

    void search(String data);

    int getPosition(D element) throws IOException;

    int getPosition(long id) throws IOException;


    interface OnCallbackListener {

        void onDataWrapperCallback();

    }

    interface OnLoadCompleteListener<T> {
        void onLoadComplete(List<T> data);
    }

    interface DietFactory {
        Diet getDiet();
    }
}
