package antonchainikov.android.trainingdiary.diarydata;

import android.database.Cursor;

import java.io.IOException;


abstract class CustomCursorWrapper<T> {

    static final String NULL_CURSOR_ERR_MSG = "Created with a null cursor, thats wrong man";

    protected Cursor mCursor;

    private final static String TAG = "CustomCursorWrapper";

    CustomCursorWrapper(Cursor cursor) {
        mCursor = cursor;
    }

    public T get(int pos) throws IOException {
        if (mCursor != null) {
            if (pos < 0 || pos >= mCursor.getCount()) {
                throw  new IOException("Argument value is negative " +
                        "or bigger then the row count in the cursor");
            } else if (!mCursor.isClosed() && mCursor.moveToPosition(pos)) {
                return getItemToReturn();
            }
        }
        throw new IOException("Error while reading database");
    }

    public int getPosition(long id, int columnIndex) throws IOException{
        if (mCursor == null || mCursor.getCount() == 0) {
            throw new IOException("The cursor is empty");
        }
        mCursor.moveToFirst();
        int rowCounter = 0;
        do {
            if (mCursor.getLong(columnIndex) == id) {
                return rowCounter;
            }
            rowCounter++;
        } while (mCursor.moveToNext());
        return -1;
    }

    public void close() {
        if (mCursor != null){
            mCursor.close();
        }
    }


    public boolean isClosed() {
        return mCursor == null || mCursor.isClosed();
    }


    public int getCount() {
        if (mCursor != null && !mCursor.isClosed()) {
            return mCursor.getCount();
        }
        return 0;
    }

    Cursor getCursor(){
        return mCursor;
    }

    abstract T getItemToReturn();

}
