package antonchainikov.android.trainingdiary.diarydata;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.IOException;

class DateDataset extends Dataset<Long> {

    private final static String TAG = "DateDataset";

    private SQLiteDatabase mDatabase;

    DateDataset(CustomAsyncLoader<Long> loader,
                SQLiteDatabase database,
                OnCallbackListener listener) {

        super(loader, listener);
        mDatabase = database;
    }
    @Override
    public Long get(int pos) throws IOException {
        if (mCursorWrapper == null || mCursorWrapper.isClosed()) {
            throw new IOException("Cursor is closed or haven't been initialized");
        }
        return mCursorWrapper.get(pos);
    }

    @Override
    public void getAll(OnLoadCompleteListener<Long> listener) {

    }


    @Override
    public long add(Long date) throws IOException {
        long rowId = -1;
        if (date != null) {
            rowId = mDatabase.insert(
                    DietDBContract.DietTable.DIET_TABLE_NAME,
                    null,
                    getContentValues(date)
            );
            update();
        }
        if (rowId == -1) {
            throw new IOException(ADD_ERR_MSG);
        }
        return rowId;
    }

    @Override
    public void delete(long date) {
        String[] args = {Long.toString(date)};
        mDatabase.delete(
                DietDBContract.DietTable.DIET_TABLE_NAME,
                DietDBContract.DietTable.Cols.DATE + " = ?",
                args
        );
        update();
    }

    @Override
    public void delete(Long element) {
        if (element != null){
            delete(element.longValue());
        }
    }

    @Override
    public int getPosition(Long element) throws IOException {
        if (element == null) {
            return -1;
        }
        return getPosition(element.longValue());
    }

    @Override
    public int getPosition(long id) throws IOException {
        return ((DateCursorWrapper)mCursorWrapper).getPosition(id);
    }

    @Override
    DateCursorWrapper getInBackground(){
        String[] columns = {DietDBContract.DietTable.Cols.DATE};
        Cursor cursor = null;
        if (mDatabase != null) {
            cursor = mDatabase.query(
                    DietDBContract.DietTable.DIET_TABLE_NAME,
                    columns,
                    null,
                    null,
                    DietDBContract.DietTable.Cols.DATE,
                    null,
                    DietDBContract.DietTable.Cols.TIME_STAMP);
        }
        return DateCursorWrapper.makeFromCursor(cursor);
    }

    private static ContentValues getContentValues(long date){
        ContentValues values = new ContentValues();
        values.put(DietDBContract.DietTable.Cols.TIME_STAMP, 0);
        values.put(DietDBContract.DietTable.Cols.DATE, date);
        values.put(DietDBContract.DietTable.Cols.FOOD_ID, 0);
        values.put(DietDBContract.DietTable.Cols.AMOUNT, 0);
        return values;
    }

}
