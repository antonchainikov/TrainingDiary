package antonchainikov.android.trainingdiary.diarydata;

import java.io.IOException;

public class Diet {

    private static final long DEFAULT_VALUE = 123;

    private long mDate;
    private double mCalories;
    private double mProtein;
    private double mFat;
    private double mCarbs;


    Diet(long date){
        mDate = date;
        mCalories = 0;
        mProtein = 0;
        mFat = 0;
        mCarbs = 0;
    }

    Diet(long date, NutritionWithAmountCursorWrapper cursorWrapper)
            throws IllegalStateException, IOException{
        this(date);
        if ( cursorWrapper == null ) {
            throw new IllegalStateException("The cursor haven't been initialized");
        } else if ( !cursorWrapper.isClosed() ) {
            for (int i = 0, rowCount = cursorWrapper.getCount(); i < rowCount; i++){
                addToCurrentDiet(cursorWrapper.get(i));
            }
        }
    }

    public static Diet getStub() {
        return new Diet(DEFAULT_VALUE);
    }

    public long getDate() {
        return mDate;
    }

    public void setDate(long millis) {
        if ( millis < 0 ) {
            mDate = System.currentTimeMillis();
        } else {
            mDate = millis;
        }
    }

    void addToCurrentDiet(Food food){
        if (food != null) {
            double amount = food.getAmount() / 100;
            mCalories += (food.getCalories() * amount);
            mProtein += (food.getProtein() * amount);
            mFat += (food.getFat() * amount);
            mCarbs += (food.getCarbs() * amount);
        }
    }

    void removeFromCurrentDiet(Food food){
        if (food != null) {
            double amount = food.getAmount() / 100;
            mCalories -= food.getCalories() * amount;
            mProtein -= food.getProtein() * amount;
            mFat -= food.getFat() * amount;
            mCarbs -= food.getCarbs() * amount;
        }
    }

    public double getCalories() {
        return mCalories;
    }

    public double getProtein() {
        return mProtein;
    }

    public double getFat() {
        return mFat;
    }

    public double getCarbs() {
        return mCarbs;
    }
}
