package antonchainikov.android.trainingdiary.diarydata;

import android.provider.BaseColumns;

class DietDBContract {

    static final String DATABASE_NAME = "nutrition.db";
    static final int DATABASE_VERSION = 1;

    static final String QUERY_JOIN_ID = "SELECT * FROM " +
            DietTable.DIET_TABLE_NAME +
            " INNER JOIN " +
            NutritionTable.NUTRITION_TABLE_NAME +
            " ON " +
            NutritionTable.NUTRITION_TABLE_NAME +"." + NutritionTable.Cols._ID +
            " = " +
            DietTable.DIET_TABLE_NAME + "." + DietTable.Cols.FOOD_ID +
            " where " +
            DietTable.DIET_TABLE_NAME + "." + DietTable.Cols.DATE + " = ? ";




    class NutritionTable {

        static final String NUTRITION_TABLE_NAME = "nutrition_table";

        static final String QUERY_CREATE_TABLE = "create table "+ NUTRITION_TABLE_NAME + "( " +
                Cols._ID +" integer primary key, " +
                Cols.NAME + " text, " +
                Cols.PROTEIN + " real, " +
                Cols.FAT + " real, " +
                Cols.CARBS + " real, " +
                Cols.CALORIES+ " real, " +
                Cols.SEARCH_COL_NAME + " text " +
                ")";


        class Cols implements BaseColumns {
            static final String NAME = "name";
            static final String CALORIES = "calories";
            static final String PROTEIN = "protein";
            static final String FAT = "fat";
            static final String CARBS = "carbs";
            static final String SEARCH_COL_NAME = "search_col_" + NAME;
        }
    }

    class DietTable {

        static final String DIET_TABLE_NAME = "diet_table";

        static final String QUERY_CREATE_TABLE = "create table " + DIET_TABLE_NAME + "( " +
                Cols._ID + " integer primary key, " +
                Cols.TIME_STAMP +" integer, " +
                Cols.FOOD_ID + " integer, " +
                Cols.DATE +  " integer, " +
                Cols.AMOUNT +  " real" +
                " )";

        class Cols implements BaseColumns{
            static final String TIME_STAMP = "time_stamp";
            static final String DATE = "date";
            static final String FOOD_ID = "food_id";
            static final String AMOUNT = "amount";
        }
    }
}
