package antonchainikov.android.trainingdiary;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment {

    private final static String ARG_DATE = "date";

    private DatePicker mDatePicker;
    private DatePickListener mListener;

    public static DatePickerFragment newInstance(long date, DatePickListener listener){
        Bundle args = new Bundle();
        args.putLong(ARG_DATE, date);
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setArguments(args);
        fragment.setListener(listener);
        return fragment;
    }

    public void setListener(DatePickListener listener){
        mListener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog( Bundle savedInstanceState){
        long millis = getArguments().getLong(ARG_DATE);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        View v = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_date, null);
        mDatePicker = (DatePicker) v.findViewById(R.id.dialog_date_picker);

        mDatePicker.init(year, month, day, null);

        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle(R.string.date_picker_caption)
                .setPositiveButton(android.R.string.ok, new OkListener())
                .setNegativeButton(android.R.string.cancel, null)
                .create();
    }

    private void sendResult(long date){        
        mListener.onDatePicked(date);
    }

    private class OkListener implements Dialog.OnClickListener{
        @Override
        public void onClick(DialogInterface dialog, int which) {
            int year = mDatePicker.getYear();
            int month = mDatePicker.getMonth();
            int day = mDatePicker.getDayOfMonth();
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            calendar.set(Calendar.MILLISECOND, 0);
            sendResult(calendar.getTimeInMillis());
        }
    }

}
