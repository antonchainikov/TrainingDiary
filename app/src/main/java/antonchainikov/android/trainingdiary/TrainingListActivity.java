package antonchainikov.android.trainingdiary;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;


public class TrainingListActivity extends SingleFragmentActivity {

    public static Intent makeStartIntent(Context context){
        return new Intent(context, TrainingListActivity.class);
    }
    @Override
    protected Fragment createFragment() {
        return new TrainingListFragment();
    }
}
