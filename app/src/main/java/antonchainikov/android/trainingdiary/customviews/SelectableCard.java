package antonchainikov.android.trainingdiary.customviews;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;


import antonchainikov.android.trainingdiary.R;


public class SelectableCard extends CardView {

    private final static String TAG = "SelectableCard";

    private float mDefaultElevation;

    public SelectableCard(@NonNull Context context) {
        this(context, null, 0);
    }

    public SelectableCard(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SelectableCard(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mDefaultElevation = getElevation();
        }
    }

    @Override
    public void setActivated(boolean activate) {
        boolean activated = isActivated();
        if ( !activated && activate ) {
            setBackgroundColor(getResources().getColor(R.color.cardview_dark_background));
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                setElevation(mDefaultElevation/2);
            }
            super.setActivated(true);
        }
        else if (activated && !activate) {
            setBackgroundColor(getResources().getColor(R.color.cardview_light_background));
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                setElevation(mDefaultElevation);
            }
            super.setActivated(false);
        }
    }
}
