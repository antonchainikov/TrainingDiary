package antonchainikov.android.trainingdiary.diarydata;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class TrainingProgramDatasetTest {
    private final static long DEFAULT_ID = 12345;
    private final static String DEFAULT_TEXT = "n/a";

    private TrainingProgramDataset testSubject;
    @Mock
    private CustomAsyncLoader<TrainingProgram> mockLoader;
    @Mock
    private SQLiteDatabase mockDatabase;
    @Mock
    private DataWrapper.OnCallbackListener mockListener;
    @Mock
    private CustomCursorWrapper<TrainingProgram> mockCursorWrapper;
    @Mock
    private TrainingProgram mockTrainingProgram;
    @Mock
    private Cursor mockCursor;

    @Before
    public void setUp() {
        testSubject = new TrainingProgramDataset(mockLoader, mockDatabase, mockListener);
        setCursorWrapperToTestSubject(mockCursorWrapper);
    }

    @Test
    public void testGet() throws Exception {
        when(mockCursorWrapper.get(anyInt())).thenReturn(mockTrainingProgram);
        assertEquals(mockTrainingProgram, testSubject.get(0));
    }

    @Test
    public void testGetWhenClosedCursor() throws Exception {
        Throwable caughtEx = null;
        when(mockCursorWrapper.isClosed()).thenReturn(true);
        try {
            testSubject.get(0);
        } catch (IOException e) {
            caughtEx = e;
        }
        assertNotNull(caughtEx);
    }

    @Test
    public void testGetWhenNullCursor() throws Exception {
        Throwable caughtEx = null;
        setCursorWrapperToTestSubject(null);
        try {
            testSubject.get(0);
        } catch (IOException e) {
            caughtEx = e;
        }
        assertNotNull(caughtEx);
    }

    @Test
    public void testAdd() throws Exception {
        TrainingProgram program = new TrainingProgram(DEFAULT_TEXT, DEFAULT_TEXT);
        testSubject.add(program);
        verify(mockDatabase).insert(
                eq(TrainingDBContract.ProgramTable.TABLE_NAME),
                nullable(String.class),
                any(ContentValues.class));
    }

    @Test
    public void testAddNull() throws Exception {
        Throwable unexpectedException = null;
        try {
            testSubject.add(null);
        } catch (Throwable t) {
            unexpectedException = t;
        }
        assertNull(unexpectedException);
    }

    @Test
    public void testDelete() throws Exception {
        //TODO implement test
    }

    @Test
    public void testDeleteNull() throws Exception {
        Throwable unexpectedException = null;
        try {
            testSubject.delete(null);
        } catch (Throwable t) {
            unexpectedException = t;
        }
        assertNull(unexpectedException);
    }

    @Test
    public void getInBackground() throws Exception {
        when(mockDatabase.query(eq(TrainingDBContract.ProgramTable.TABLE_NAME),
                nullable(String[].class), nullable(String.class), nullable(String[].class),
                nullable(String.class), nullable(String.class), nullable(String.class)))
                .thenReturn(mockCursor);
        TrainingCursorWrapper result = testSubject.getInBackground();
        assertNotNull(result);
    }

    private void setCursorWrapperToTestSubject(CustomCursorWrapper<TrainingProgram> cursor) {
        testSubject.onLoadComplete(mockLoader, cursor);
    }

}