package antonchainikov.android.trainingdiary.diarydata;

import android.support.v4.content.Loader;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("unchecked call")
@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class DatasetTest {

    private final static int DEFAULT_CURSOR_ROW_COUNT = 5;

    @Mock
    private CustomCursorWrapper mockCursorWrapper;

    @Mock
    private DataWrapper.OnCallbackListener mocklistener;

    @Mock
    private CustomAsyncLoader mockLoader;

    private Dataset mTestsubject;
    private int timesUpdateCalled;


    @Before
    public void setUp(){
        mTestsubject = new Dataset( mockLoader, mocklistener) {

            @Override
            public long add(Object element) throws IOException {
                return 0;
            }

            @Override
            public void delete(long id) {

            }

            @Override
            public void delete(Object element) {

            }

            @Override
            public int getPosition(Object element) throws IOException {
                return 0;
            }

            @Override
            public int getPosition(long id) throws IOException {
                return 0;
            }

            @Override
            public void onLoadComplete(Loader loader, Object data) {

            }

            @Override
            CustomCursorWrapper getInBackground() {
                return null;
            }

        };
        timesUpdateCalled = 1;
        when(mockCursorWrapper.getCount()).thenReturn(DEFAULT_CURSOR_ROW_COUNT);
        setSubjectsCursorWrapperToMock();
    }


    @Test
    public void testSizeMethod() {
        assertEquals(DEFAULT_CURSOR_ROW_COUNT, mTestsubject.size());
        when(mockCursorWrapper.isClosed()).thenReturn(true);
        assertEquals(0, mTestsubject.size());
        setSubjectsCursorWrapperToNull();
        assertEquals(0, mTestsubject.size());
        verify(mockCursorWrapper, times(2)).isClosed();
    }

    @Test
    public void testUpdateMethod() {
        mTestsubject.update();
        verify(mockLoader, times(timesUpdateCalled + 1)).forceLoad();
        setSubjectsLoaderToNull();
        mTestsubject.update();
    }

    @Test
    public void testCloseMethod() {
        mTestsubject.close();
        verify(mockCursorWrapper).close();
        verify(mockLoader).cancelLoad();
    }

    @Test
    public void testOnLoadComplete() {
        CustomCursorWrapper newCursor = Mockito.mock(CustomCursorWrapper.class);
        when(newCursor.getCount()).thenReturn(DEFAULT_CURSOR_ROW_COUNT + 1);
        mTestsubject.onLoadComplete(null, newCursor);
        assertEquals(DEFAULT_CURSOR_ROW_COUNT + 1, mTestsubject.size());
        verify(mockCursorWrapper).close();
        mTestsubject.onLoadComplete(null, null);
    }

    private void setSubjectsCursorWrapperToNull(){
        mTestsubject.onLoadComplete(null, null);
    }

    private void setSubjectsCursorWrapperToMock(){
        mTestsubject.onLoadComplete(null, mockCursorWrapper);
    }

    private void setSubjectsLoaderToNull(){
        mTestsubject = new Dataset( null, mocklistener) {
            @Override
            CustomCursorWrapper getInBackground() {
                return null;
            }
        };
    }

}