package antonchainikov.android.trainingdiary.diarydata;

import android.database.Cursor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;


@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class DateCursorWrapperTest {
    // TODO rewrite tests they're not up to date 'cause some methods are implemented by supercalss and tested there

    private static final int DATE_COLUMN_INDEX = 2; // DietDBContract.DietTable.Cols.DATE
    private static final long MOCK_DATE = 12345L;
    private static final int ROWS_IN_CURSOR = 10;

    @Mock
    private Cursor mMockCursor;
    private DateCursorWrapper mTestSubject;


    @Before
    public void setup(){
        when(mMockCursor.getColumnIndex(DietDBContract.DietTable.Cols.DATE))
                .thenReturn(DATE_COLUMN_INDEX);
        when(mMockCursor.getCount()).thenReturn(ROWS_IN_CURSOR);
        when(mMockCursor.getLong(DATE_COLUMN_INDEX)).thenReturn(MOCK_DATE);
        mTestSubject = DateCursorWrapper.makeFromCursor(mMockCursor);
        MockitoAnnotations.initMocks(mTestSubject);

    }

    @Test
    public void getDateSuccessful() throws Exception {
        // successfully move to position
        when(mMockCursor.moveToPosition(anyInt())).thenReturn(true);
        assertEquals(mTestSubject.get(0).longValue(), MOCK_DATE);
        assertEquals(mTestSubject.get(5).longValue(), MOCK_DATE);
        assertEquals(mTestSubject.get(ROWS_IN_CURSOR-1).longValue(), MOCK_DATE);

    }

    @Test
    public void getDateFailed()  throws Exception {
        int exceptionCounter = 0;
        when(mMockCursor.moveToPosition(anyInt())).thenReturn(false);
        List<Integer> caseList = new LinkedList<>();
        caseList.add(-1);
        caseList.add(ROWS_IN_CURSOR);
        caseList.add(ROWS_IN_CURSOR + 1);
        caseList.add(5);
        for (Integer pos: caseList){
            try {
                mTestSubject.get(pos);
            } catch (IOException e){
                exceptionCounter++;
            }
        }
        assertEquals(exceptionCounter, caseList.size());
    }

    @Test
    public void testFactoryMethod() throws Exception {
        DateCursorWrapper result = DateCursorWrapper.makeFromCursor(null);
        assertNotNull(result);
        result = DateCursorWrapper.makeFromCursor(mMockCursor);
        assertNotNull(result);
    }

}