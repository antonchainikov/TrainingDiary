package antonchainikov.android.trainingdiary.diarydata;




import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.compat.BuildConfig;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

import antonchainikov.android.trainingdiary.R;

import static org.junit.Assert.*;


@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, manifest = Config.NONE)
public class DiaryDatabaseTest {

    private static int INSTANTIATION_TEST_RUNS = 10;

    private DiaryDatabase mTestSubject;
    private  Context mContext;

    @Before
    public void setupDatabase(){
        mContext = RuntimeEnvironment.application;
        assertNotNull(mContext);
        mTestSubject = DiaryDatabase.getInstance(mContext);
    }

    @After
    public void tearDown(){
        if (mTestSubject != null) {
            mTestSubject.close();
        }
        TestUtils.resetSingleton(DiaryDatabase.class, "sInstance");
    }

    @Test
    public void testInstantiation(){
        final DiaryDatabase[] results = new DiaryDatabase[INSTANTIATION_TEST_RUNS];
        final CountDownLatch latch = new CountDownLatch(INSTANTIATION_TEST_RUNS);
        final CyclicBarrier barrier = new CyclicBarrier(INSTANTIATION_TEST_RUNS);

        class InstantiationThread extends Thread{
            private final int mIndex;
            private InstantiationThread(int index){
                mIndex = index;
            }
            public void run(){
                try {
                    barrier.await();
                } catch (Exception e){
                    throw new RuntimeException();
                }
                results[mIndex] = DiaryDatabase.getInstance(mContext);
                latch.countDown();
            }
        }

        TestUtils.resetSingleton(DiaryDatabase.class, "sInstance");
        for (int i = 0; i < INSTANTIATION_TEST_RUNS; i++){
            new InstantiationThread(i).start();
        }
        try{
            latch.await();
        } catch (InterruptedException e){
            throw new RuntimeException();
        }
        DiaryDatabase res = results[0];
        assertNotNull(res);
        if (INSTANTIATION_TEST_RUNS > 1){
            for (int i = 1; i < INSTANTIATION_TEST_RUNS; i++){
                assertEquals(results[i], res);
            }
        }
    }


    // test Database creation + 2 tables
    // NutritionTable
    // DietTable

    @Test
    public void testOnCreate(){
        SQLiteDatabase database = mTestSubject.getWritableDatabase();
        assertNotNull(database);
        TestUtils.testTableCreation(database, DietDBContract.NutritionTable.NUTRITION_TABLE_NAME);
        TestUtils.testTableCreation(database, DietDBContract.DietTable.DIET_TABLE_NAME);
    }

    @Test
    public void testFillNutritionTable(){
        InputStream is = mContext.getResources().openRawResource(R.raw.nutrition_raw);
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(is)
        );
        int linesInRawFile = 0;
        try {
            while ( reader.readLine() != null ){
                linesInRawFile++;
            }
        } catch (IOException e){
            throw new AssertionError();
        }
        Cursor testCursor = TestUtils.getAllDataCursor(
                mTestSubject.getReadableDatabase(),
                DietDBContract.NutritionTable.NUTRITION_TABLE_NAME);
        while (mTestSubject.isFillingTable()){
            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e){
                //NVM
            }
        }
        System.out.println(testCursor.getCount());
        assertEquals(linesInRawFile, testCursor.getCount());
        testCursor.close();
    }

    @Test
    public void testGetFoodDataset(){
        assertNotNull(mTestSubject.getFoodDataset(null));
    }

    @Test
    public void testGetDietDataset(){
        assertNotNull(mTestSubject.getDietDataset(null));
    }

    @Test
    public void testGetFoodDatasetForDay(){
        assertNotNull(mTestSubject.getFoodDatasetForDay(null, 0));
    }
}