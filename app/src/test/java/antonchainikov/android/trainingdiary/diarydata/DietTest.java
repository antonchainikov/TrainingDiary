package antonchainikov.android.trainingdiary.diarydata;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class DietTest {

    private final static long DEFAULT_DATE = 12345L;
    private final static int DEFAULT_CURSOR_ROW_COUNT = 0;
    private final static String FIRST_FOOD_NAME = "First Food";
    private final static String SECOND_FOOD_NAME = "Second Food";

    private final static double FIRST_FOOD_DEFAULT_VALUES = 10;
    private final static double FIRST_FOOD_DEFAULT_AMOUNT = 100;
    private final static double SECOND_FOOD_DEFAULT_VALUES = 20;
    private final static double SECOND_FOOD_DEFAULT_AMOUNT = 200;

    private static double amountN1 = FIRST_FOOD_DEFAULT_AMOUNT / 100;
    private static double amountN2 = SECOND_FOOD_DEFAULT_AMOUNT / 100;

    private Food foodN1;
    private Food foodN2;



    private Diet testSubject;
    @Mock
    private NutritionWithAmountCursorWrapper mockCursor;

    @Before
    public void setUp()throws IOException{
        foodN1 = new Food(
                0,
                FIRST_FOOD_NAME,
                FIRST_FOOD_DEFAULT_VALUES,
                FIRST_FOOD_DEFAULT_VALUES,
                FIRST_FOOD_DEFAULT_VALUES,
                FIRST_FOOD_DEFAULT_VALUES
        );
        foodN1.setAmount(FIRST_FOOD_DEFAULT_AMOUNT);

        foodN2 = new Food(
                1,
                SECOND_FOOD_NAME,
                SECOND_FOOD_DEFAULT_VALUES,
                SECOND_FOOD_DEFAULT_VALUES,
                SECOND_FOOD_DEFAULT_VALUES,
                SECOND_FOOD_DEFAULT_VALUES
        );
        foodN2.setAmount(SECOND_FOOD_DEFAULT_AMOUNT);

        when(mockCursor.getCount()).thenReturn(DEFAULT_CURSOR_ROW_COUNT);
        //when(mockCursor.get(0)).thenReturn(foodN1);
        //when(mockCursor.get(1)).thenReturn(foodN2);
        testSubject = new Diet(DEFAULT_DATE, mockCursor);
        testSubject.addToCurrentDiet(foodN1);
        testSubject.addToCurrentDiet(foodN2);
    }

    @Test
    public void getDate() throws Exception {
        assertEquals(DEFAULT_DATE, testSubject.getDate());
    }

    @Test
    public void setNegativeDate() throws Exception {
        testSubject.setDate(-1);
        assertTrue( testSubject.getDate() >= 0 );
    }

    @Test
    public void setDate() throws Exception {
        long date = 54321;
        testSubject.setDate(date);
        assertEquals(date, testSubject.getDate());
    }

    @Test
    public void removeFromCurrentDiet() throws Exception {
        testSubject.removeFromCurrentDiet(foodN1);
        testSubject.removeFromCurrentDiet(foodN2);
        assertEquals(0, testSubject.getProtein(), 0.01);
        assertEquals(0, testSubject.getFat(), 0.01);
        assertEquals(0, testSubject.getCarbs(), 0.01);
        assertEquals(0, testSubject.getCalories(), 0.01);
    }

    @Test
    public void removeNullFromCurrentDiet() throws Exception {
        testSubject.removeFromCurrentDiet(null);
    }

    @Test
    public void addNullToCurrentDiet() throws Exception {
        testSubject.removeFromCurrentDiet(null);
    }

    @Test
    public void getCalories() throws Exception {
        double caloriesExpected =
                foodN1.getCalories() * amountN1 + foodN2.getCalories() * amountN2;
        assertEquals(caloriesExpected, testSubject.getCalories(), 0.01);
    }

    @Test
    public void getProtein() throws Exception {
        double proteinExpected =
                foodN1.getProtein() * amountN1 + foodN2.getProtein() * amountN2;
        assertEquals(proteinExpected, testSubject.getProtein(), 0.01);
    }

    @Test
    public void getFat() throws Exception {
        double fatExpected =
                foodN1.getFat() * amountN1 + foodN2.getFat() * amountN2;
        assertEquals(fatExpected, testSubject.getFat(), 0.01);
    }

    @Test
    public void getCarbs() throws Exception {
        double carbsExpected =
                foodN1.getCarbs() * amountN1 + foodN2.getCarbs() * amountN2;
        assertEquals(carbsExpected, testSubject.getCarbs(), 0.01);
    }

    @Test
    public void nullCursorInConstructor() throws Exception {
        try {
            testSubject = new Diet(DEFAULT_DATE, null);
        } catch (IllegalStateException e) {
            assertTrue(e.getMessage().contains("cursor"));
        }
    }

    @Test
    public void closedCursorInConstructor() throws Exception {
        when(mockCursor.isClosed()).thenReturn(true);
        testSubject = new Diet(DEFAULT_DATE, mockCursor);
        verify(mockCursor, times(0)).get(anyInt());
    }

}