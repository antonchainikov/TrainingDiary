package antonchainikov.android.trainingdiary.diarydata;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class ExerciseDatasetTest {

    private ExerciseDataset testSubject;
    @Mock
    private CustomAsyncLoader<Exercise> mockLoader;
    @Mock
    private SQLiteDatabase mockDatabase;
    @Mock
    private DataWrapper.OnCallbackListener mockListener;
    @Mock
    private CustomCursorWrapper<Exercise> mockCursorWrapper;
    @Mock
    private Exercise mockExercise;
    @Mock
    private Cursor mockCursor;


    @Before
    public void setUp() {
        testSubject = new ExerciseDataset(mockLoader, mockDatabase, mockListener);
        setCursorWrapperToTestSubject(mockCursorWrapper);
    }

    @Test
    public void testGet() throws Exception {
        when(mockCursorWrapper.get(anyInt())).thenReturn(mockExercise);
        assertEquals(mockExercise, testSubject.get(0));
    }

    @Test
    public void testGetWhenClosedCursor() throws Exception {
        Throwable caughtEx = null;
        when(mockCursorWrapper.isClosed()).thenReturn(true);
        try {
            testSubject.get(0);
        } catch (IOException e) {
            caughtEx = e;
        }
        assertNotNull(caughtEx);
    }

    @Test
    public void testGetWhenNullCursor() throws Exception {
        Throwable caughtEx = null;
        setCursorWrapperToTestSubject(null);
        try {
            testSubject.get(0);
        } catch (IOException e) {
            caughtEx = e;
        }
        assertNotNull(caughtEx);
    }

    @Test
    public void add() throws Exception {
        //TODO implement method
    }

    @Test
    public void delete() throws Exception {
        //TODO implement method
    }

    @Test
    public void testGetInBackground() throws Exception {
        when(mockDatabase.query(eq(TrainingDBContract.ExerciseTable.TABLE_NAME),
                nullable(String[].class), nullable(String.class), nullable(String[].class),
                nullable(String.class), nullable(String.class), nullable(String.class)))
                .thenReturn(mockCursor);
        ExerciseCursorWrapper result = testSubject.getInBackground();
        assertNotNull(result);
    }

    private void setCursorWrapperToTestSubject(CustomCursorWrapper<Exercise> cursor) {
        testSubject.onLoadComplete(mockLoader, cursor);
    }

}