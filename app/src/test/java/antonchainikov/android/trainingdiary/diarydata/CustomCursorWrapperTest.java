package antonchainikov.android.trainingdiary.diarydata;

import android.database.Cursor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class CustomCursorWrapperTest {

    private final static String CORRECT_ANSWER = "this is the Answer";
    private final static int ROWS_IN_CURSOR = 10;

    private CustomCursorWrapper testSubject;
    @Mock
    private Cursor mockCursor;


    @Before
    public void setUp() {
        testSubject = new CustomCursorWrapper<String>(mockCursor) {
            @Override
            String getItemToReturn() {
                return CORRECT_ANSWER;
            }
        };
        when(mockCursor.moveToPosition(anyInt())).thenReturn(true);
        when(mockCursor.getCount()).thenReturn(ROWS_IN_CURSOR);
    }


    @Test
    public void testGetCorrectInput() throws Exception {
        assertTrue(testSubject.get(ROWS_IN_CURSOR - 1).equals(CORRECT_ANSWER));
        assertTrue(testSubject.get(0).equals(CORRECT_ANSWER));
        assertTrue(testSubject.get(ROWS_IN_CURSOR / 2).equals(CORRECT_ANSWER));
    }

    @Test
    public void testGetIndexOutOfBounds() throws Exception {

        // testing index out of bounds cases
        int exceptionCounter = 0;
        List<Integer> caseList = new LinkedList<>();
        caseList.add(-1);
        caseList.add(ROWS_IN_CURSOR);
        caseList.add(ROWS_IN_CURSOR + 1);
        for (Integer pos: caseList){
            try {
                testSubject.get(pos);
            } catch (IOException e){
                if (e.getMessage().contains("Argument value")) {
                    exceptionCounter++;
                }
            }
        }
        assertEquals(exceptionCounter, caseList.size());
    }

    @Test // testing failed cursor.moveToPosition()
    public void testGetFailedMoveToPosition() throws Exception {
        when(mockCursor.moveToPosition(anyInt())).thenReturn(true);
        try {
            testSubject.get(ROWS_IN_CURSOR / 2);
        } catch (IOException e){
            assertTrue(e.getMessage().contains("Error while reading database"));
        }
    }

    @Test  // testing case when the cursor is null
    public void testGetNullCursor() throws Exception {
        setCursorInTestSubjectToNull();
        try {
            testSubject.get(ROWS_IN_CURSOR / 2);
        } catch (IOException e) {
            assertTrue(e.getMessage().contains("Error while reading database"));
        }
    }

    @Test  // testing case when the cursor is null
    public void testGetWhileClosedCursor() throws Exception {
        when(mockCursor.isClosed()).thenReturn(true);
        try {
            testSubject.get(ROWS_IN_CURSOR / 2);
        } catch (IOException e) {
            assertTrue(e.getMessage().contains("Error while reading database"));
        }
    }

    @Test
    public void testCloseCorrect() throws Exception {
        testSubject.close();
        verify(mockCursor).close();
    }

    @Test
    public void testCloseNullCursor() throws Exception {
        setCursorInTestSubjectToNull();
        testSubject.close();
    }

    @Test // it should be open by default return value in mockCursor
    public void testIsClosedCorrect() throws Exception {
        assertFalse(testSubject.isClosed());
        when(mockCursor.isClosed()).thenReturn(true);
        assertTrue(testSubject.isClosed());
    }

    @Test
    public void testIsClosedNullCursor() throws Exception {
        setCursorInTestSubjectToNull();
        assertTrue(testSubject.isClosed());
    }

    @Test
    public void getCountCorrect() throws Exception {
        assertEquals(ROWS_IN_CURSOR, testSubject.getCount());
    }

    @Test
    public void getCountClosedCursor() throws Exception {
        when(mockCursor.isClosed()).thenReturn(true);
        assertEquals(0, testSubject.getCount());
    }

    @Test
    public void getCountNullCursor() throws Exception {
        setCursorInTestSubjectToNull();
        assertEquals(0, testSubject.getCount());
    }

    @Test
    public void getCursor() throws Exception {
        assertEquals(mockCursor, testSubject.getCursor());
        setCursorInTestSubjectToNull();
        assertNull(testSubject.getCursor());
    }

    private void setCursorInTestSubjectToNull() {
        testSubject = new CustomCursorWrapper<String>(null) {
            @Override
            String getItemToReturn() {
                return CORRECT_ANSWER;
            }
        };
    }
}