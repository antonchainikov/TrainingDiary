package antonchainikov.android.trainingdiary.diarydata;

import android.database.Cursor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class ExerciseCursorWrapperTest {

    private ExerciseCursorWrapper testSubject;
    @Mock
    private Cursor mockCursor;

    @Before
    public void setUp() {
        when(mockCursor.getColumnIndex(anyString())).thenReturn(0);
        when(mockCursor.getInt(anyInt())).thenReturn(0);
        when(mockCursor.getString(anyInt())).thenReturn("n/a");
        when(mockCursor.getDouble(anyInt())).thenReturn(0D);
    }

    @Test
    public void testGetItemToReturnWithPlan() throws Exception {
        when(mockCursor.getColumnIndexOrThrow(anyString())).thenReturn(0);
        testSubject = new ExerciseCursorWrapper(mockCursor, true);
        Exercise exercise = testSubject.getItemToReturn();
        assertNotNull(exercise.getExercisePlan());
    }

    @Test
    public void testGetItemToReturnWithoutPlan() throws Exception {
        testSubject = new ExerciseCursorWrapper(mockCursor, false);
        Exercise exercise = testSubject.getItemToReturn();
        assertNull(exercise.getExercisePlan());
    }

    @Test
    public void testGetItemToReturnWithPlanWrong() throws Exception {
        when(mockCursor.getColumnIndexOrThrow(anyString())).thenThrow(new IllegalArgumentException());
        testSubject = new ExerciseCursorWrapper(mockCursor, true);
        Exercise exercise = testSubject.getItemToReturn();
        assertNull(exercise.getExercisePlan());
    }

}