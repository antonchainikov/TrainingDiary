package antonchainikov.android.trainingdiary.diarydata;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class FoodDatasetTest {

    private final static long DEFAULT_ID = 12345;

    private FoodDataset testSubject;
    @Mock
    private CustomAsyncLoader<Food> mockLoader;
    @Mock
    private SQLiteDatabase mockDatabase;
    @Mock
    private DataWrapper.OnCallbackListener mockListener;
    @Mock
    private CustomCursorWrapper<Food> mockCursorWrapper;
    @Mock
    private Food mockFood;
    @Mock
    private Cursor mockCursor;

    @Before
    public void setUp() {
        testSubject = new FoodDataset(mockLoader, mockDatabase, mockListener);
        setCursorWrapperToTestSubject(mockCursorWrapper);
    }

    @Test
    public void testGet() throws Exception {
        when(mockCursorWrapper.get(anyInt())).thenReturn(mockFood);
        assertEquals(mockFood, testSubject.get(0));
    }

    @Test
    public void testGetWhenClosedCursor() throws Exception {
        Throwable caughtEx = null;
        when(mockCursorWrapper.isClosed()).thenReturn(true);
        try {
            testSubject.get(0);
        } catch (IOException e) {
            caughtEx = e;
        }
        assertNotNull(caughtEx);
    }

    @Test
    public void testGetWhenNullCursor() throws Exception {
        Throwable caughtEx = null;
        setCursorWrapperToTestSubject(null);
        try {
            testSubject.get(0);
        } catch (IOException e) {
            caughtEx = e;
        }
        assertNotNull(caughtEx);
    }

    @Test
    public void testAdd() throws Exception {
        Food food = new Food(DEFAULT_ID, "n/a", 0,0,0,0);
        testSubject.add(food);
        verify(mockDatabase).insert(
                eq(DietDBContract.NutritionTable.NUTRITION_TABLE_NAME),
                nullable(String.class),
                any(ContentValues.class));
    }

    @Test
    public void testAddNull() throws Exception {
        Throwable unexpectedException = null;
        try {
            testSubject.add(null);
        } catch (Throwable t) {
            unexpectedException = t;
        }
        assertNull(unexpectedException);
    }

    @Test
    public void testDelete() throws Exception {
        testSubject.delete(DEFAULT_ID);
        verify(mockDatabase).delete(
                anyString(),
                anyString(),
                any(String[].class));
    }

    @Test
    public void testDeleteNull() throws Exception {
        Throwable unexpectedException = null;
        try {
            testSubject.delete(null);
        } catch (Throwable t) {
            unexpectedException = t;
        }
        assertNull(unexpectedException);
    }

    @Test
    public void getInBackground() throws Exception {
        when(mockDatabase.query(eq(DietDBContract.NutritionTable.NUTRITION_TABLE_NAME),
                nullable(String[].class), nullable(String.class), nullable(String[].class),
                nullable(String.class), nullable(String.class), nullable(String.class)))
                .thenReturn(mockCursor);
        NutritionCursorWrapper result = testSubject.getInBackground();
        assertNotNull(result);
    }

    private void setCursorWrapperToTestSubject(CustomCursorWrapper<Food> cursor) {
        testSubject.onLoadComplete(mockLoader, cursor);
    }
}