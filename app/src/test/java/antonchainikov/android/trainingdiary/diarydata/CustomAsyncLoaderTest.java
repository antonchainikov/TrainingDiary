package antonchainikov.android.trainingdiary.diarydata;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("unchecked call")
@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class CustomAsyncLoaderTest {

    private CustomAsyncLoader mTestSubject;

    @Mock
    private Dataset mockDataset;

    @Mock
    private CustomCursorWrapper mockCursor;

    @Mock
    private Context mockContext;

    @Before
    public void setUp(){
        when(mockContext.getApplicationContext()).thenReturn(mockContext);
        mTestSubject = new CustomAsyncLoader(mockContext);
    }

    @Test
    public void testOnAttachToDataset() {
        mTestSubject.onAttachToDataset(mockDataset);
        mTestSubject.loadInBackground();
        verify(mockDataset).getInBackground();
    }


    @Test
    public void testLoadInBackground() {
        when(mockDataset.getInBackground()).thenReturn(mockCursor);
        CustomCursorWrapper result = mTestSubject.loadInBackground();
        assertNull(result);
        mTestSubject.onAttachToDataset(mockDataset);
        result = mTestSubject.loadInBackground();
        assertEquals(mockCursor, result);
    }

}