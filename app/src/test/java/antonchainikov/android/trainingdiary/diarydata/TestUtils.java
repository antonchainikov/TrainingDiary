package antonchainikov.android.trainingdiary.diarydata;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.lang.reflect.Field;

public class TestUtils {

    public static void resetSingleton(Class clazz, String fieldName) {

        Field instance;
        try {
            instance = clazz.getDeclaredField(fieldName);
            instance.setAccessible(true);
            instance.set(null, null);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    public static void testTableCreation(SQLiteDatabase database, String tableName)
            throws AssertionError{
        Cursor testCursor = getAllDataCursor(database, tableName);
        if (testCursor == null){
            throw new AssertionError();
        } else {
            testCursor.close();
        }
    }

    public static Cursor getAllDataCursor(SQLiteDatabase database, String tableName){
        return database.query(
                tableName,
                null,
                null,
                null,
                null,
                null,
                null
        );
    }

}
