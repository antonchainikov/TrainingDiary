package antonchainikov.android.trainingdiary.diarydata;

import android.database.Cursor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class TrainingCursorWrapperTest {

    private final static String DEFAULT_TEXT = "n/a";
    private final static int DEFAULT_NUM = 0;

    private TrainingCursorWrapper testSubject;
    @Mock
    private Cursor mockCursor;

    @Before
    public void setUp() {
        testSubject = new TrainingCursorWrapper(mockCursor);
        when(mockCursor.getLong(anyInt())).thenReturn((long)DEFAULT_NUM);
        when(mockCursor.getString(anyInt())).thenReturn(DEFAULT_TEXT);
    }

    @Test
    public void getItemToReturn() throws Exception {
        TrainingProgram program = new TrainingProgram(DEFAULT_TEXT, DEFAULT_TEXT);
        TrainingProgram result = testSubject.getItemToReturn();
        assertTrue(program.equals(result));
    }

    @Test
    public void testNullCursorConstructor() {
        Throwable unexpectedException = null;
        try {
            testSubject = new TrainingCursorWrapper(null);
        } catch (Throwable t) {
            unexpectedException = t;
        }
        assertNull(unexpectedException);
    }

}