package antonchainikov.android.trainingdiary.diarydata;

import android.database.Cursor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class NutritionWithAmountCursorWrapperTest {

    private final static String DEFAULT_NAME = "n/a";
    private final static int DEFAULT_NUM = 0;

    private NutritionWithAmountCursorWrapper testSubject;
    @Mock
    private Cursor mockCursor;

    @Before
    public void setUp() {
        testSubject = new NutritionWithAmountCursorWrapper(mockCursor);
        when(mockCursor.getInt(anyInt())).thenReturn(DEFAULT_NUM);
        when(mockCursor.getString(anyInt())).thenReturn(DEFAULT_NAME);
        when(mockCursor.getDouble(anyInt())).thenReturn((double)DEFAULT_NUM);
    }

    @Test
    public void getItemToReturn() throws Exception {
        Food food = new Food(
                DEFAULT_NUM,
                DEFAULT_NAME,
                DEFAULT_NUM,
                DEFAULT_NUM,
                DEFAULT_NUM,
                DEFAULT_NUM
        );
        food.setAmount(DEFAULT_NUM);
        Food result = testSubject.getItemToReturn();
        assertTrue(food.equals(result));
    }

    @Test
    public void testNullCursorConstructor() {
        Throwable unexpectedException = null;
        try {
            testSubject = new NutritionWithAmountCursorWrapper(null);
        } catch (Throwable t) {
            unexpectedException = t;
        }
        assertNull(unexpectedException);
    }
}