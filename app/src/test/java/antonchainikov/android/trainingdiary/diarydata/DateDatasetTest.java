package antonchainikov.android.trainingdiary.diarydata;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class DateDatasetTest {

    private final static long TRUE_RETURN_VAL = 100L;
    private final static int INITIAL_CURSOR_LOAD = 1;

    private DateDataset mTestSubject;
    @Mock
    private SQLiteDatabase mockDatabase;
    @Mock
    private CustomAsyncLoader<Long> mockLoader;
    @Mock
    private CustomCursorWrapper<Long> mockCursor;


    @Before
    public void setUp() throws Exception{
        mTestSubject = new DateDataset(mockLoader, mockDatabase, null);
        mTestSubject.onLoadComplete(null, mockCursor);
        when(mockCursor.get(anyInt())).thenReturn(TRUE_RETURN_VAL);
    }

    @Test
    public void testGet() throws Exception {
        long result = mTestSubject.get(1);
        assertEquals(TRUE_RETURN_VAL, result);

        when(mockCursor.isClosed()).thenReturn(true);
        try {
            mTestSubject.get(1);
        } catch (IOException e){
            assertTrue(e.getMessage().contains("Cursor is closed"));
        }

        mTestSubject.onLoadComplete(null, null);
        try {
            mTestSubject.get(1);
        } catch (IOException e){
            assertTrue(e.getMessage().contains("Cursor is closed"));
        }
        verify(mockCursor, times(1)).get(anyInt());
    }


    @Test
    public void testAdd() throws Exception {
        mTestSubject.add(1L);
        mTestSubject.add(null);
        verify(mockDatabase, times(1))
                .insert(anyString(), nullable(String.class), nullable(ContentValues.class));
        verify(mockLoader, times(INITIAL_CURSOR_LOAD +1)).forceLoad();
    }

    @Test
    public void testDeleteByDate() throws Exception {
        mTestSubject.delete(5L);
        verify(mockDatabase).delete(
                eq(DietDBContract.DietTable.DIET_TABLE_NAME),
                eq(DietDBContract.DietTable.Cols.DATE + " = ?"),
                any(String[].class));
        verify(mockLoader, times(INITIAL_CURSOR_LOAD +1)).forceLoad();
    }

    @Test
    public void testDeleteByElement() throws Exception {
        mTestSubject.delete(null);
        verifyZeroInteractions(mockDatabase);
        mTestSubject.delete(new Long(5L));
        String[] arg = {Long.toString(5L)};
        verify(mockDatabase).delete(
                eq(DietDBContract.DietTable.DIET_TABLE_NAME),
                eq(DietDBContract.DietTable.Cols.DATE + " = ?"),
                eq(arg));
    }

    @Test
    public void testGetInBackground() throws Exception {
        Cursor cursor = Mockito.mock(Cursor.class);
        when(mockDatabase.query(
                anyString(),
                nullable(String[].class),
                nullable(String.class),
                nullable(String[].class),
                nullable(String.class),
                nullable(String.class),
                nullable(String.class))).thenReturn(cursor);
        CustomCursorWrapper result = mTestSubject.getInBackground();
        assertNotNull(result);
    }

}